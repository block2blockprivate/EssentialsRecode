package net.raptorpvp.RaptorPvPCore;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import net.raptorpvp.RaptorPvPCore.commands.*;
import net.raptorpvp.RaptorPvPCore.listeners.*;
import net.raptorpvp.RaptorPvPCore.listeners.sneaks.*;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.permission.Permission;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

import net.raptorpvp.RaptorPvPCore.MySQL.MySQLManager;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

public final class Main extends JavaPlugin implements PluginMessageListener {
    
    private static Main instance;

    private static Map<String, Location> parkourCheckpoints = new HashMap<>();
    private static List<Hologram> holograms = new ArrayList<>();

    private static File configFile;
    private static FileConfiguration config;

    private static File parkourFile;
    private static FileConfiguration parkour;

    public static Permission perms = null;

    public MySQLManager mysql;

    public Plugin pl = this;

    public static FileConfiguration getParkourFile() { return parkour; }
    public static Location getParkourLocation(String path) {return ((Location) parkour.get(path));}

    public static List<Hologram> getHolograms() {return holograms;}

    private void loadParkourConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        parkourFile = new File(getDataFolder(), "parkour.yml");
        if (!parkourFile.exists()) {
            parkourFile.getParentFile().mkdirs();
            copy(getResource("parkour.yml"), parkourFile);
        }
        parkour = new YamlConfiguration();
        loadYamls();
    }


    public void loadYamls() {
        try {
            parkour.load(parkourFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveYamls() {

        if (config.get("leaderboard.1.UUID").equals("N/A")) {
            config.set("leaderboard.1.UUID", "N/A");
            config.set("leaderboard.1.PlayerName", "No-one");
            config.set("leaderboard.1.Time", 2147483646.0);
        }
        if (config.get("leaderboard.2.UUID").equals("N/A")) {
            config.set("leaderboard.2.UUID", "N/A");
            config.set("leaderboard.2.PlayerName", "No-one");
            config.set("leaderboard.2.Time", 2147483646.0);
        }
        if (config.get("leaderboard.3.UUID").equals("N/A")) {
            config.set("leaderboard.3.UUID", "N/A");
            config.set("leaderboard.3.PlayerName", "No-one");
            config.set("leaderboard.3.Time", 2147483646.0);
        }

        try {
            parkour.save(parkourFile);
            parkour = YamlConfiguration.loadConfiguration(parkourFile);
            config.save(configFile);
            config = YamlConfiguration.loadConfiguration(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addParkourConfig(String path, Object value) {
        parkour.set(path, value);
        try {
            parkour.save(parkourFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        parkour = YamlConfiguration.loadConfiguration(parkourFile);
    }



    public static int totalChecks() {
        int i = 0;
       for (String s : getParkourFile().getKeys(false)) {
            if (s.equalsIgnoreCase("end")||s.equalsIgnoreCase("spawn")||s.equalsIgnoreCase("reset")) {
                continue;
            }
            i++;
       }
       return i;
    }

    private void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
        if (subchannel.equals("ServerSecurity")) {
            String p = in.readUTF();
            JoinListener.allowedUsers.add(p);
        }
    }

    @Override
    public void onEnable() {
        if (!Bukkit.getMessenger().isOutgoingChannelRegistered(this, "BungeeCord")) {
            Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        }
        if (!Bukkit.getMessenger().isIncomingChannelRegistered(this, "ServerSecurity")) {
            Bukkit.getMessenger().registerIncomingPluginChannel(this, "ServerSecurity", this);
        }
        if(Bukkit.getServer().getPluginManager().isPluginEnabled("HolographicDisplays"))
        instance = this;
        getLogger().info("RaptorPvP-Core has been successfully enabled!");
        loadConfig();
        loadParkourConfig();
        registerTimers();
        registerCommands();
        try {
            registerListeners(new JoinListener(), new LeaveListener(), new EnderSneak(), new JacobSneak(), new BrandonSneak(),
                    new BaronSneak(), new BlockSneak(), new ChatSlow(), new Forcefield(), new WorldProtection(), new Parkour(),new InventoryClickListener(), new CommandSpyListener(), new Ignore(), new InventoryClickMainInv(),new PlayerIneractOpenGUI(),new InvClickStaffGUI(),new InvClickServerNav(), new WeatherListener(), new InvClickPrefMenu());
        } catch (Throwable e) {
            e.printStackTrace();
            for (StackTraceElement st : e.getStackTrace()) {
                Bukkit.getServer().broadcastMessage("Line " + st.getLineNumber() + "\n" + st.toString());
            }
        }
        registerListeners(new ItemFrameProtector());
        setupPermissions();

        mysql = new MySQLManager();
        try {
            mysql.setup();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (getConfig().getBoolean("settings.hubmode")) {
            for (String s : getParkourFile().getKeys(false)) {
                if (s.equalsIgnoreCase("spawn")) {
                    Location l = (Location) getParkourFile().get("spawn.location");
                    l.setX(l.getX() + 0.5);
                    l.setZ(l.getZ() + 0.5);
                    l.setY(l.getY() + 2);
                    Hologram hologram = HologramsAPI.createHologram(this, l);
                    TextLine textLine = hologram.appendTextLine(org.bukkit.ChatColor.BLUE + "" + org.bukkit.ChatColor.BOLD + "Parkour Start");
                    holograms.add(hologram);
                    l.setX(l.getX() - 0.5);
                    l.setZ(l.getZ() - 0.5);
                    l.setY(l.getY() - 2);
                    Parkour.setStart(l);
                } else if (s.equalsIgnoreCase("end")) {
                    Location l = (Location) getParkourFile().get("end.location");
                    l.setY(l.getY() + 2);
                    l.setX(l.getX() + 0.5);
                    l.setZ(l.getZ() + 0.5);
                    Hologram hologram = HologramsAPI.createHologram(this, l);
                    TextLine textLine = hologram.appendTextLine(org.bukkit.ChatColor.BLUE + "" + org.bukkit.ChatColor.BOLD + "Parkour End");
                    holograms.add(hologram);
                    l.setY(l.getY() - 2);
                    l.setX(l.getX() - 0.5);
                    l.setZ(l.getZ() - 0.5);
                    Parkour.setEnd(l);
                } else if (s.equalsIgnoreCase("reset")) {
                    continue;
                } else {
                    Location l = (Location) getParkourFile().get(s + ".location");
                    l.setY(l.getY() + 2);
                    l.setX(l.getX() + 0.5);
                    l.setZ(l.getZ() + 0.5);
                    Hologram hologram = HologramsAPI.createHologram(this, l);
                    TextLine textLine = hologram.appendTextLine(org.bukkit.ChatColor.BLUE + "" + org.bukkit.ChatColor.BOLD + "Checkpoint #" + s);
                    holograms.add(hologram);
                    l.setY(l.getY() - 2);
                    l.setX(l.getX() - 0.5);
                    l.setZ(l.getZ() - 0.5);
                    Parkour.getCheckLocations().add(l);
                }
            }
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("RaptorPvPCore has been successfully disabled!");
        for (Hologram hologram : holograms) {
            hologram.delete();
        }
    }

    private void registerListeners(Listener... listeners) {
        Arrays.stream(listeners).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));

        for (TimerEvent.TimerType t : TimerEvent.TimerType.values()) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    getServer().getPluginManager().callEvent(new TimerEvent(t));
                }
            }.runTaskTimer(this, 0, t.getTicks());
        }
    }

    private void registerTimers() {
        /*
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(TICK));
            }
        }, 0, 0);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(HALF));
            }
        }, 0, 10);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(SECOND));
            }
        }, 0, 20);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(DOUBLE));
            }
        }, 0, 40);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(TRIPLE));
            }
        }, 0, 60);
        */
    }

    public static FileConfiguration getMainConfig() {
        return config;
    }

    private void loadConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);

        //Register Defaults
        Map<String, Object> map = new HashMap<>();
        map.put("settings.enableSneaks", false);
        map.put("settings.hubmode",false);
        map.put("leaderboard.3.Time", 2147483646.0);
        map.put("leaderboard.3.UUID", "N/A");
        map.put("leaderboard.3.PlayerName", "No-one");
        map.put("leaderboard.2.Time", 2147483646.0);
        map.put("leaderboard.2.UUID", "N/A");
        map.put("leaderboard.2.PlayerName", "No-one");
        map.put("leaderboard.1.Time", 2147483646.0);
        map.put("leaderboard.1.UUID", "N/A");
        map.put("leaderboard.1.PlayerName", "No-one");

        config.addDefaults(map);
        config.options().copyDefaults(true);
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addConfig(String path, Object value) {
        config.set(path, value);
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private void registerCommands() {
        getCommand("gm").setExecutor(new CommandGM());
        getCommand("fly").setExecutor(new CommandFly());
        getCommand("feed").setExecutor(new CommandFeed());
        getCommand("heal").setExecutor(new CommandHeal());
        getCommand("kill").setExecutor(new CommandKill());
        getCommand("burn").setExecutor(new CommandBurn());
        getCommand("tp").setExecutor(new CommandTP());
        getCommand("give").setExecutor(new CommandGive());
        getCommand("clear").setExecutor(new CommandClear());
        getCommand("hat").setExecutor(new CommandHat());
        getCommand("time").setExecutor(new CommandTime());
        getCommand("bc").setExecutor(new CommandBc());
        getCommand("chatslow").setExecutor(new CommandChat());
        CommandRadius cr = new CommandRadius();
        getCommand("forcefield").setExecutor(cr);
        getCommand("radius").setExecutor(cr);
        getCommand("setrank").setExecutor(new CommandSetrank());
        getCommand("rules").setExecutor(new CommandRules());
        getCommand("skull").setExecutor(new CommandSkull());
        getCommand("lightning").setExecutor(new CommandLightning());
        getCommand("commandspy").setExecutor(new CommandCommandSpy());
        getCommand("staff").setExecutor(new CommandStaff());
        getCommand("staffmessage").setExecutor(new CommandStaffMessage());
        getCommand("staffreply").setExecutor(new CommandStaffReply());
        getCommand("parkour").setExecutor(new Parkour());
        getCommand("preferences").setExecutor(new CommandPrefs());
    }
    
    public static Main get() {
        return instance;
    }

    public static String c(String prefix, String message) {
        return ChatColor.translateAlternateColorCodes('&', ((prefix==null)?"&7":"&2"+prefix+">> &7") + message);
    }

    public static String cr(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static void broadcast(String prefix, String message) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', ((prefix==null)?"&7":"&2"+prefix+">> &7") + message));
        }
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

}
