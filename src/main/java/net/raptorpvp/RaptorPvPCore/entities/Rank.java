package net.raptorpvp.RaptorPvPCore.entities;

import net.raptorpvp.RaptorPvPCore.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import org.bukkit.OfflinePlayer;

public enum Rank {

    PLAYER("Player", "&f", "f", "2", 0, false, false, null, null, ChatColor.RESET),
    IMMORTAL("Immortal", "&d&l[Immortal] ", "f", "d", 2, false, true, "http://store.raptorpvp.net/", "&d&l[Immortal]\n \n" +
            "&fThis rank is very rare, and only\n" +
            "&fthe most daring of players will\n" +
            "&fventure into it's unknown value!\n \n" +
            "&aClick to visit the store!", ChatColor.RESET),
    OVERLORD("Overlord", "&b&l[Overlord] ", "f", "b", 1, false, true, "http://store.raptorpvp.net/", "&b&l[Overlord]\n \n" +
            "&fThis rank is uncommon, and is\n" +
            "&fonly found in the Realm of the\n" +
            "Unknown! It's value is incomparable!\n \n" +
            "&aClick to visit the store!", ChatColor.RESET),
    SFE("SFE", "&5&l[SFE] ", "f", "5", 3, false, true, "http://sfe.raptorpvp.net/",  "&5&l[SFE]\n \n" +
            "&fThis rank is given to staff members\n" +
            "&fon the Something For Everyone Discord\n" +
            "server!\n \n" +
            "&aClick to join the Discord!", ChatColor.RESET),
    YOUTUBER("YouTuber", "&c&l[YouTuber] ", "f", "c", 4, false, true, "http://forums.raptorpvp.net/index.php?threads/youtuber-and-youtuber-requirements-application.78/", "&c&l[YouTuber]\n\n" +
            "&fThis rank is given to YouTube\n" +
            "&fcontent creators on RaptorPvP.\n \n" +
            "&aClick to view rank requirements.", ChatColor.RESET),
    BUILDER("Builder", "&3&l[Builder] ", "f", "b", 5, false, false, null, "&3&l[Builder]\n \n" +
            "&fBuilders create and fix all of the\n" +
            "&fmaps you can find on the network!", ChatColor.RESET),
    ARTIST("Artist", "&3&l[Artist] ", "f", "b", 6, false, false, null, "&3&l[Artist]\n \n" +
            "&fArtists produce all of the artwork\n" +
            "&fyou can find around the network!", ChatColor.RESET),
    HELPER("Helper", "&a&l[Helper] ", "a", "2", 7, true, false, null, "&a&l[Helper]\n \n" +
            "&fHelpers answer any questions or\n" +
            "&fqueries players have, as well as\n" +
            "&fmoderate the network. Helpers have\n" +
            "&fless permissions than Moderators.", ChatColor.GREEN),
    MODERATOR("Moderator", "&6&l[Mod] ", "e", "6", 8, true, false, null, "&6&l[Mod]\n \n" +
            "&fMods answer any questions or\n" +
            "&fqueries players have, as well as\n" +
            "&fmoderate the network.", ChatColor.YELLOW),
    ADMIN("Admin", "&c&l[Admin] ", "c", "4", 9, true, false, null, "&c&l[Admin]\n \n" +
            "&fAdmins monitor their specific teams,\n" +
            "&fmaking sure all of the staff inside those\n" +
            "&fteams are working efficiently and to the\n" +
            "&fbest of their ability.", ChatColor.RED),
    DEVELOPER("Developer", "&a&l[Dev] ", "a", "2", 10, true, false, null, "&a&l[Dev]\n \n" +
            "&fDevelopers create the content that\n" +
            "&fyou see on all our servers! They work\n" +
            "&fbehind the scenes coding the games you\n" +
            "&flove to play!", ChatColor.GREEN),
    MANAGER("Manager", "&4&l[Manager] ", "c", "4", 11, true, false, null, "&4&l[Manager]\n \n" +
            "&fManagers make sure the server\n" +
            "&fis running efficiently, monitoring\n" +
            "things like the staff team, and new\n" +
            "content and feedback on updates.", ChatColor.RED),
    OWNER("Owner", "&4&l[Owner] ", "c", "4", 9001, true, false, null, "&4&l[Owner]\n \n" +
            "&fOwners manage all aspects of\n" +
            "&fthe network, keeping an eye on\n" +
            "&fthe staff team and managing all\n" +
            "&fcontent that is published.", ChatColor.RED);

    private String name;
    private String displayName;
    private String chatColor;
    private String suffixColor;
    private int id;
    private boolean isStaff;
    private boolean clickable;
    private String clickableText;
    private String hoverText;
    private ChatColor colour;

    Rank(String name, String displayName, String chatColor, String suffixColor, int id, boolean isStaff, boolean clickable, String clickableText, String hoverText, ChatColor colour) {
        this.name = name;
        this.displayName = displayName;
        this.chatColor = chatColor;
        this.suffixColor = suffixColor;
        this.id = id;
        this.isStaff = isStaff;
        this.clickable = clickable;
        this.clickableText = clickableText;
        this.hoverText = hoverText;
        this.colour = colour;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getChatColor() {
        return chatColor;
    }

    public String getSuffixColor() {
        return suffixColor;
    }

    public int getId() {
        return id;
    }

    public boolean isStaff() {
        return isStaff;
    }

    public BaseComponent formatChat(String name, String message) {
        TextComponent rank = new TextComponent(Main.cr(this.displayName));
        if (this.hoverText != null) {
            ComponentBuilder hoverText = new ComponentBuilder(Main.cr(this.hoverText));
            rank.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText.create()));
        }
        if (this.clickable)
            rank.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,this.clickableText));

        TextComponent formattedName = new TextComponent(Main.cr("&r" + name));
        TextComponent formattedText = new TextComponent(Main.cr(" &r&" + this.suffixColor + "»&r &" + this.chatColor + message));
        formattedText.setColor(this.colour);
        formattedName.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,"/msg " + name + " "));
        TextComponent finale = new TextComponent("");
        finale.addExtra(rank);
        finale.addExtra(formattedName);
        finale.addExtra(formattedText);
        return finale;
    }

    public static Rank getGroup(OfflinePlayer p) {
        String group = Main.perms.getPrimaryGroup("hub", p);
        for (Rank rank : values()) {
            if (rank.getName().equals(group)) {
                return rank;
            }
        }
        return PLAYER;
    }

    public static boolean compare(Rank first, Rank second) {
        return first.getId()>=second.getId();
    }

    public static Rank getByName(String name) {
        for (Rank rank : values()) {
            if (rank.getName().toUpperCase().equals(name.toUpperCase()))
                return rank;
        }
        return null;
    }

    public String getClickableText() {
        return clickableText;
    }

    public String getHoverText() {
        return hoverText;
    }

    public ChatColor getColour() {
        return colour;
    }
}
