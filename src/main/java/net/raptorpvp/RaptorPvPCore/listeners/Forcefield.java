package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.commands.CommandRadius;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;


public class Forcefield implements Listener {

    private static Map<Entity, Integer> charge = new HashMap<>();

    public Forcefield() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Entity e : charge.keySet()) {
                    int i = charge.get(e);
                    if (i>0) {
                        charge.put(e, i - 1);
                    } else {
                        charge.remove(e);
                    }
                }
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    if (CommandRadius.map.containsKey(p)) {
                        int radius = CommandRadius.getRadius(p);
                        for (Player t : Bukkit.getServer().getOnlinePlayers()) {
                            if (t == p) continue;
                            if (t.hasPermission("rank.admin")) continue;
                            if (t.getLocation().toVector().subtract(p.getLocation().toVector()).length() > radius) continue;
                            push(p, t);
                        }
                    }
                }
            }
        }.runTaskTimer(Main.get(), 0, 0);
    }

    private void push(Entity p, Entity t) {
        if (charge(p)) {
            t.getWorld().playSound(t.getLocation(), Sound.CHICKEN_EGG_POP, 2f, 0.5f);
            Vector vec = t.getLocation().toVector().subtract(p.getLocation().toVector()).setY(0);
            if (Double.isNaN(vec.getX()) || Double.isNaN(vec.getY()) || Double.isNaN(vec.getZ()) || vec.length() == 0)
                return;
            vec.setY(vec.getY() + 2);
            vec.normalize();
            vec.multiply(1.8);
            if (vec.getY() > 5)
                vec.setY(5);
            t.setFallDistance(0);
            t.setVelocity(vec);
        }
    }


    private boolean charge(Entity e) {
        if (charge.containsKey(e)) return false; else {
            charge.put(e, 10);
            return true;
        }
    }
}
