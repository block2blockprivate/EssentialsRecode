package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.commands.CommandRadius;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.bukkit.Material.EMERALD_BLOCK;
import static org.bukkit.Material.REDSTONE_BLOCK;

public class InvClickPrefMenu implements Listener {

    static Main m = Main.get();

    public static List<Player> visibility = new ArrayList<>();
    public static List<Player> invisible = new ArrayList<>();

    @EventHandler
    public void onClickServerNav(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player p = (Player) e.getWhoClicked();
        String invname;
        try {
            invname = ChatColor.stripColor(e.getClickedInventory().getName());
        } catch (NullPointerException n) {
            return;
        }
        String name = ChatColor.stripColor(invname);
        if (name.equalsIgnoreCase("Your Preferences")) {
            e.setCancelled(true);
            int slot = e.getSlot();
            if (slot < 9)
                return;
            ItemStack item = e.getInventory().getItem(slot-9);
            if (item == null)
                return;
            String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
            try {
                switch (itemName.toLowerCase()) {
                    case "player chat":
                        if (Main.getMainConfig().getBoolean("prefs." + e.getWhoClicked().getUniqueId() + ".chat")) {
                            Ignore.disabled.add((Player) e.getWhoClicked());
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".chat", false);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7your chat."));
                        } else {
                            Ignore.disabled.remove((Player) e.getWhoClicked());
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".chat", true);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7your chat."));
                        }
                        break;
                    case "speed":
                        if (Main.getMainConfig().getBoolean("prefs." + e.getWhoClicked().getUniqueId() + ".speed")) {
                            e.getWhoClicked().removePotionEffect(PotionEffectType.SPEED);
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".speed", false);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7speed."));
                        } else {
                            e.getWhoClicked().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000, 2));
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".speed", true);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7speed."));
                        }
                        break;
                    case "flight":
                        if (((Player) e.getWhoClicked()).getAllowFlight()) {
                            ((Player) e.getWhoClicked()).setAllowFlight(false);
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7flight."));
                        } else {
                            ((Player) e.getWhoClicked()).setAllowFlight(true);
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7flight."));
                        }
                        break;
                    case "player visibility":
                        if (!visibility.contains(e.getWhoClicked())) {
                            visibility.add((Player) e.getWhoClicked());
                            for (Player p2 : Bukkit.getOnlinePlayers()) {
                                if (p2 == e.getWhoClicked())
                                    continue;
                                ((Player) e.getWhoClicked()).hidePlayer(p2);
                            }
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".visibility", false);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7players."));
                        } else {
                            visibility.remove(e.getWhoClicked());
                            for (Player p2 : Bukkit.getOnlinePlayers()) {
                                if (p2 == e.getWhoClicked())
                                    continue;
                                ((Player) e.getWhoClicked()).showPlayer(p2);
                            }
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".visibility", true);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7players."));
                        }
                        break;
                    case "radius":
                        if (CommandRadius.map.containsKey(e.getWhoClicked())) {
                            CommandRadius.map.remove(e.getWhoClicked());
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".radius", false);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7radius."));
                        } else {
                            CommandRadius.map.put((Player) e.getWhoClicked(), 5);
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".radius", true);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7radius."));
                        }
                        break;
                    case "invisibility":
                        if (invisible.contains(e.getWhoClicked())) {
                            invisible.remove(e.getWhoClicked());
                            for (Player p2 : Bukkit.getOnlinePlayers()) {
                                if (p2 == e.getWhoClicked())
                                    continue;
                                if (p2.hasPermission("rank.staff"))
                                    continue;
                                p2.showPlayer((Player) e.getWhoClicked());
                            }
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".invisibility", false);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &cdisabled &7invisibility."));
                        } else {
                            invisible.add((Player) e.getWhoClicked());
                            for (Player p2 : Bukkit.getOnlinePlayers()) {
                                if (p2 == e.getWhoClicked())
                                    continue;
                                if (p2.hasPermission("rank.staff"))
                                    continue;
                                p2.hidePlayer((Player) e.getWhoClicked());
                            }
                            Inventory inv = e.getClickedInventory();
                            i(inv, e.getSlot(), EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                            e.getWhoClicked().openInventory(inv);
                            Main.addConfig("prefs." + e.getWhoClicked().getUniqueId() + ".invisibility", true);
                            e.getWhoClicked().sendMessage(Main.c("Preferences", "You &aenabled &7invisibility."));
                        }
                        break;
                    default:
                        break;
                }
            } catch (Exception ex) {
                return;
            }
        }
    }

    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing, String skullName) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Main.c(null, name));
        if (lore != null) {
            im.setLore(Arrays.asList(Main.c(null, lore).split(";")));
        }
        if (glowing) {
            im.addEnchant(Enchantment.DURABILITY, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        is.setItemMeta(im);
        inv.setItem(slot, is);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing) {
        i(inv, slot, type, name, amount, lore, data, glowing, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data) {
        i(inv, slot, type, name, amount, lore, data, false);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore) {
        i(inv, slot, type, name, amount, lore, (short)0);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount) {
        i(inv, slot, type, name, amount, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name) {
        i(inv, slot, type, name, 1);
    }
    private static void i(Inventory inv, int slot, Material type) {
        i(inv, slot, type, "");
    }

}
