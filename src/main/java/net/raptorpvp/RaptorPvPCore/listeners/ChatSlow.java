package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.commands.CommandChat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class ChatSlow implements Listener {

    private Map<Player, Integer> map = new HashMap<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void chat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        if (!p.hasPermission("rank.staff")) {
            if (CommandChat.isSilenced()) {
                e.setCancelled(true);
                p.sendMessage(Main.c("ChatSlow", "Chat is currently disabled."));
            } else {
                int a = CommandChat.getSlow();
                if (map.containsKey(p)) {
                    int b = map.get(p);
                    if (b > 0) {
                        e.setCancelled(true);
                        p.sendMessage(Main.c("ChatSlow", "Chat slow is enabled, please wait " + b + " more seconds."));
                        return;
                    }
                }
                map.put(p, a);
            }
        }
    }

    public ChatSlow() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (CommandChat.getSlow()>0) {
                    for (Player p : map.keySet()) {
                        map.put(p, map.get(p)-1);
                    }
                } else {
                    map.clear();
                }
            }
        }.runTaskTimer(Main.get(),0, 20);
    }


}
