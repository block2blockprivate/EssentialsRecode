package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.MySQL.MySQLManager;
import net.raptorpvp.RaptorPvPCore.ScoreboardManager;
import net.raptorpvp.RaptorPvPCore.commands.CommandRadius;
import net.raptorpvp.RaptorPvPCore.entities.Rank;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.*;

public class JoinListener implements Listener{

	private static Main m = Main.get();

	private static Map<Player, List<String>> listOfIgnored = new HashMap<>();
	public static List<String> allowedUsers = new ArrayList<>();
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
	    if (!Main.getMainConfig().getBoolean("players.tablecreated." + e.getPlayer().getUniqueId().toString())) {
	        try {
                MySQLManager.createTable(e.getPlayer().getUniqueId().toString());
            } catch (NullPointerException e2) {
            }
            Main.addConfig("players.tablecreated." + e.getPlayer().getUniqueId().toString(), true);
        }
        if (listOfIgnored.containsKey(e.getPlayer())) {
	        listOfIgnored.remove(e.getPlayer());
        }
        listOfIgnored.put(e.getPlayer(),MySQLManager.getIgnored(e.getPlayer().getUniqueId().toString()));
		e.setJoinMessage(Main.c(null, "&7[&a+&7] &f"+e.getPlayer().getName()));
		if (Main.getMainConfig().getBoolean("settings.hubmode")) {
            Scoreboard s = ScoreboardManager.sm.getNewScoreboard();
            Objective o = s.registerNewObjective(e.getPlayer().getName(), "dummy");
            o.setDisplaySlot(DisplaySlot.SIDEBAR);
            o.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&9&m----&b&l RAPTORPVP &9&m----"));
            ScoreboardManager.getMap().put(e.getPlayer(), o);
            ScoreboardManager.getScoreboardMap().put(e.getPlayer(), s);
            e.getPlayer().getInventory().clear();
            e.getPlayer().getInventory().setHelmet(null);
            e.getPlayer().getInventory().setChestplate(null);
            e.getPlayer().getInventory().setLeggings(null);
            e.getPlayer().getInventory().setBoots(null);
            ItemStack i = new ItemStack(Material.COMPASS,1);
            ItemMeta im = i.getItemMeta();
            im.setDisplayName(Main.c(null,"&b&lServer Navigation"));
            im.setLore(Arrays.asList(Main.c(null, "Right-click this item to show all of the servers that you can join.").split(",")));
            i.setItemMeta(im);
            e.getPlayer().getInventory().setItem(0,i);
            ItemStack i3 = new ItemStack(Material.REDSTONE_COMPARATOR,1);
            ItemMeta im3 = i3.getItemMeta();
            im3.setDisplayName(Main.c(null,"&b&lPreferences"));
            im3.setLore(Arrays.asList(Main.c(null, "Right-click this item to open your preferences menu.").split(",")));
            i3.setItemMeta(im3);
            e.getPlayer().getInventory().setItem(4,i3);
            if (e.getPlayer().hasPermission("rank.staff")||e.getPlayer().hasPermission("rank.builder")) {
                ItemStack i2 = new ItemStack(Material.EMERALD,1);
                ItemMeta im2 = i2.getItemMeta();
                im2.setDisplayName(Main.c(null,"&b&lStaff/Test Servers"));
                im2.setLore(Arrays.asList(Main.c(null, "Right-click this item to show all of the staff/test servers available.").split(",")));
                i2.setItemMeta(im2);
                e.getPlayer().getInventory().setItem(1,i2);
            }
            e.getPlayer().teleport(new Location(Bukkit.getServer().getWorld("hub"),0.5,75,0.5,0,0));
            setScoreboard(e.getPlayer());
            if (!Main.getMainConfig().isSet("prefs." + e.getPlayer().getUniqueId() + ".chat")) {
                Main.addConfig("prefs." + e.getPlayer().getUniqueId() + ".chat", true);
                Main.addConfig("prefs." + e.getPlayer().getUniqueId() + ".speed", false);
                Main.addConfig("prefs." + e.getPlayer().getUniqueId() + ".visibility", true);
                Main.addConfig("prefs." + e.getPlayer().getUniqueId() + ".radius", false);
                Main.addConfig("prefs." + e.getPlayer().getUniqueId() + ".invisibility", false);
            }

            if (!Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".chat")) {
                Ignore.disabled.add(e.getPlayer());
            }
            if (Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".speed")) {
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000, 2));
            }
            if (Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".radius")) {
                CommandRadius.map.put(e.getPlayer(), 5);
            }
            if (!Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".visibility")) {
                InvClickPrefMenu.visibility.add(e.getPlayer());
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (e.getPlayer() == p)
                        continue;
                    e.getPlayer().hidePlayer(p);
                }
            }
            if (Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".invisibility")) {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (e.getPlayer() == p)
                        continue;
                    if (p.hasPermission("rank.staff"))
                        continue;
                    p.hidePlayer(e.getPlayer());
                    InvClickPrefMenu.invisible.add(e.getPlayer());
                }
            }
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (!Main.getMainConfig().getBoolean("prefs." + p.getUniqueId() + ".visibility")) {
                    p.hidePlayer(e.getPlayer());
                }
                if (Main.getMainConfig().getBoolean("prefs." + p.getUniqueId() + ".invisibility")) {
                    p.hidePlayer(e.getPlayer());
                }
            }
        }
		OfflinePlayer p = (OfflinePlayer) e.getPlayer();
		m.addConfig("players." + p.getName().toLowerCase(), p);
	}

	public static Map<Player, List<String>> getMap() {
	    return listOfIgnored;
    }

    public static void setScoreboard(Player p) {
	    ScoreboardManager.changeLine(p, 15, Main.cr("&3» &b&lServer"));
        ScoreboardManager.changeLine(p, 14, "Hub");
        ScoreboardManager.changeLine(p, 13, " ");
        ScoreboardManager.changeLine(p, 12, Main.cr("&3» &b&lRank"));
        ScoreboardManager.changeLine(p, 11, Rank.getGroup((OfflinePlayer) p).getName());
        ScoreboardManager.changeLine(p, 10, "  ");
        ScoreboardManager.changeLine(p, 9, Main.cr("&3» &b&lPlayers Online"));
        ScoreboardManager.changeLineGlobal(8, "" + Bukkit.getOnlinePlayers().size());
        ScoreboardManager.changeLine(p, 7, "   ");
        ScoreboardManager.changeLine(p, 6, Main.cr("&3» &b&lStaff Online"));
        int i = 0;
        for (Player p2 : Bukkit.getOnlinePlayers()) {
            if (p2.hasPermission("rank.staff")) {
                i++;
            }
        }
        ScoreboardManager.changeLineGlobal(5, i + " ");
        ScoreboardManager.changeLine(p, 4, "    ");
        ScoreboardManager.changeLine(p, 3, Main.cr("&3» &b&lWebsite"));
        ScoreboardManager.changeLine(p, 2, "www.raptorpvp.net");
        ScoreboardManager.changeLine(p, 1, Main.cr("&9&m--------------------"));
    }
}
