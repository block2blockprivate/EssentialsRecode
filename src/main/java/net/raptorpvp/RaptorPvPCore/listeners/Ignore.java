package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.entities.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;

public class Ignore implements Listener {

    public static List<Player> disabled = new ArrayList<>();

    @EventHandler(priority = EventPriority.HIGH)
    public void chat(AsyncPlayerChatEvent e) {
        if (e.getPlayer().hasPermission("rank.admin")) {
            e.setMessage(ChatColor.translateAlternateColorCodes('&',e.getMessage()));
        }
        e.setCancelled(true);
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!JoinListener.getMap().get(p).contains(e.getPlayer().getUniqueId().toString())||p.hasPermission("rank.staff")) {
                if (!disabled.contains(p)||e.getPlayer().hasPermission("rank.staff")||p.equals(e.getPlayer())||p.hasPermission("rank.staff")) {
                    p.spigot().sendMessage(Rank.getGroup(e.getPlayer()).formatChat(e.getPlayer().getName(), e.getMessage()));
                }
            }
        }

    }
}
