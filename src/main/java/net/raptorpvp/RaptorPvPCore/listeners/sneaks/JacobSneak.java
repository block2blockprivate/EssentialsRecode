package net.raptorpvp.RaptorPvPCore.listeners.sneaks;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: JacobRuby
 */

public class JacobSneak implements Listener {

    private List<Snowball> snowballs = new ArrayList<>();


    @EventHandler
    public void jacob(PlayerToggleSneakEvent e) {
        if (Main.get().getConfig().getBoolean("settings.enableSneaks")) {
            Player p = e.getPlayer();
            if (p.getName().equals("JacobRuby")) {
                if (e.isSneaking()) {
                    Snowball snowball = p.launchProjectile(Snowball.class, p.getLocation().getDirection());
                    snowball.setVelocity(snowball.getVelocity().multiply(0.7));
                    snowball.teleport(snowball.getLocation().add(0, 0.5, 0));
                    snowballs.add(snowball);
                    for (int i = 20; i > 0; i--) {
                        p.getWorld().playEffect(p.getLocation().add(0, 1, 0), Effect.HEART, 1, 2);
                    }
                    p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_LAUNCH, 1.5f, 1f);
                }
            }
        }
    }

    @EventHandler
    public void hit(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Snowball) {
            Snowball s = (Snowball) e.getEntity();
            if (snowballs.contains(s)) {
                Entity p = e.getEntity();
                p.getWorld().playSound(p.getLocation(), Sound.ANVIL_LAND, 1f, 0.5f);
                p.getWorld().spigot().playEffect(p.getLocation(), Effect.HAPPY_VILLAGER, 0, 0, 1f, 1f, 1f, 1f, 10, 100);
                snowballs.remove(s);
                s.remove();
            }
        }
    }

}
