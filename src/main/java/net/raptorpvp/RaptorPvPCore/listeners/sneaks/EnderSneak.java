package net.raptorpvp.RaptorPvPCore.listeners.sneaks;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Author: JacobRuby
 */


public class EnderSneak implements Listener {

    @EventHandler
    public void onEnderSneak(PlayerToggleSneakEvent e) {
        if (Main.get().getConfig().getBoolean("settings.enableSneaks")) {
            if (e.getPlayer().getName().equals("xEnderSlayer")) {
                if (e.isSneaking()) {
                    Player p = e.getPlayer();
                    int random = (int) Math.floor(Math.random() * 4);
                    switch (random) {
                        case 0:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_DEATH, 0.5f, 1.5f);
                            break;
                        case 1:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_HIT, 0.5f, 1.5f);
                            break;
                        case 2:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_SCREAM, 0.5f, 1.5f);
                            break;
                        case 3:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_SCREAM, 0.5f, 2f);
                            break;
                        case 4:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 0.5f, 1.5f);
                            break;
                        default:
                            p.getWorld().playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 0.5f, 1.5f);
                            break;
                    }
                }
            }
        }
    }

}
