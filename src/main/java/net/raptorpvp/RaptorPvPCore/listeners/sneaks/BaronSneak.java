package net.raptorpvp.RaptorPvPCore.listeners.sneaks;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class BaronSneak implements Listener {

    @EventHandler
    public void baron(PlayerToggleSneakEvent e) {
        if (Main.get().getConfig().getBoolean("settings.enableSneaks")) {
            Player p = e.getPlayer();
            if (p.getName().equals("BaronVonMechtler") && e.isSneaking()) {
                p.getWorld().playSound(p.getLocation(), Sound.WOLF_BARK, 1f, 1f);
            }
        }
    }
}
