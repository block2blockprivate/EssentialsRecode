package net.raptorpvp.RaptorPvPCore.listeners.sneaks;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Author: JacobRuby
 */

public class BrandonSneak implements Listener {

    @EventHandler
    public void brandon(PlayerToggleSneakEvent e) {
        if (Main.get().getConfig().getBoolean("settings.enableSneaks")) {
            Player p = e.getPlayer();
            if (p.getName().equals("SimplyBrandon1")) {
                if (e.isSneaking()) {
                    p.getWorld().playSound(p.getLocation(), Sound.PIG_IDLE, 0.5f, 1f);
                }
            }
        }
    }

}
