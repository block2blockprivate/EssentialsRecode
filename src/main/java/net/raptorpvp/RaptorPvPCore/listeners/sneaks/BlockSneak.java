package net.raptorpvp.RaptorPvPCore.listeners.sneaks;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class BlockSneak implements Listener {

    @EventHandler
    public void sneak(PlayerToggleSneakEvent e) {
        if (Main.get().getConfig().getBoolean("settings.enableSneaks")) {
            Player p = e.getPlayer();
            if (e.isSneaking()) {
                if (p.getName().equals("Block2Block")) {
                    act(p);
                }
            }
        }
    }

    public void act(Player p) {
        Location eye = p.getEyeLocation();
        for (int a = 6; a>0; a--) {
            double dir1 = Math.random()*2;
            double dir2 = Math.random()*2;
            Material m;
            switch ((int) Math.floor(Math.random()*5)) {
                case 0: m = Material.REDSTONE; break;
                case 1: m = Material.DIODE; break;
                case 2: m = Material.REDSTONE_BLOCK; break;
                case 3: m = Material.REDSTONE_COMPARATOR; break;
                case 4: m = Material.REDSTONE_TORCH_ON; break;
                case 5: m = Material.REDSTONE_LAMP_OFF; break;
                default:
                    m = Material.REDSTONE;
            }
            Item i = p.getWorld().dropItem(eye, new ItemStack(m));
            i.setPickupDelay(1000000);
            i.setVelocity(i.getVelocity().multiply(new Vector(dir1, 2, dir2)));
            new BukkitRunnable() {
                @Override
                public void run() {
                    i.remove();
                }
            }.runTaskLater(Main.get(), 60);
        }

    }
}
