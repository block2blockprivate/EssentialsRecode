package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.Parkour;
import net.raptorpvp.RaptorPvPCore.ScoreboardManager;
import net.raptorpvp.RaptorPvPCore.commands.CommandRadius;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffectType;

/**
 * Author: JacobRuby
 */

public class LeaveListener implements Listener {

    private static Main m = Main.get();


    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        e.setQuitMessage(Main.c(null, "&7[&c-&7] &f" + e.getPlayer().getName()));
        if (Main.getMainConfig().getBoolean("settings.hubmode")) {
            e.getPlayer().getInventory().clear();
            e.getPlayer().getInventory().setHelmet(null);
            e.getPlayer().getInventory().setChestplate(null);
            e.getPlayer().getInventory().setLeggings(null);
            e.getPlayer().getInventory().setBoots(null);

            ScoreboardManager.getMap().remove(e.getPlayer());
            ScoreboardManager.getScoreboardMap().remove(e.getPlayer());
            ScoreboardManager.changeLineGlobal(8, Bukkit.getOnlinePlayers().size() - 1 + "");
            int i = 0;
            for (Player p2 : Bukkit.getOnlinePlayers()) {
                if (p2.hasPermission("rank.staff")) {
                    i++;
                }
            }
            ScoreboardManager.changeLineGlobal(5, i - 1 + " ");

            if (Ignore.disabled.contains(e.getPlayer())) {
                Ignore.disabled.remove(e.getPlayer());
            }
            if (Main.getMainConfig().getBoolean("prefs." + e.getPlayer().getUniqueId() + ".speed")) {
                e.getPlayer().removePotionEffect(PotionEffectType.SPEED);
            }
            if (CommandRadius.map.containsKey(e.getPlayer())) {
                CommandRadius.map.remove(e.getPlayer());
            }
            if (InvClickPrefMenu.visibility.contains(e.getPlayer())) {
                InvClickPrefMenu.visibility.remove(e.getPlayer());
            }
            if (InvClickPrefMenu.invisible.contains(e.getPlayer())) {
                InvClickPrefMenu.invisible.remove(e.getPlayer());
            }
        }
        OfflinePlayer p = (OfflinePlayer) e.getPlayer();
        Main.addConfig("players." + p.getName().toLowerCase(), p);
        if (Main.getMainConfig().getBoolean("settings.hubmode")) {
            Parkour.removeParkourPlayers(e.getPlayer());
            Parkour.removeParkourPTimes(e.getPlayer());
        }
    }
}
