package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import org.apache.commons.lang.ObjectUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryClickListener implements Listener {

    private static Main m = Main.get();

    @EventHandler(priority = EventPriority.HIGH)
    public void click(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player p = (Player) e.getWhoClicked();
        String invname;
        try {
            invname = ChatColor.stripColor(e.getClickedInventory().getName());
        } catch (NullPointerException n) {
            return;
        }
        String name = ChatColor.stripColor(invname);
        if (name.matches("Set [a-zA-Z0-9_]{1,16}'s rank")) {
            e.setCancelled(true);
            if (p.hasPermission("rank.admin")) {
                int slot = e.getSlot();
                if (slot < 9)
                    return;
                ItemStack item = e.getInventory().getItem(slot);
                if (item == null)
                    return;
                String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                OfflinePlayer target = m.getConfig().getOfflinePlayer("players." + ChatColor.stripColor(e.getInventory().getItem(4).getItemMeta().getDisplayName()).toLowerCase());
                String group = Main.perms.getPrimaryGroup("hub", target);
                try {
                    if (group.equalsIgnoreCase(itemName))
                        return;
                } catch (NullPointerException e2) {
                    group = "Player";
                    if (group.equalsIgnoreCase(itemName))
                        return;
                }
                String group2 = Main.perms.getPrimaryGroup("hub", ((Player) e.getWhoClicked()).getPlayer());

                switch (group2.toLowerCase()) {
                    case "owner":
                        break;
                    case "manager":
                        if (itemName.equalsIgnoreCase("owner")||itemName.equalsIgnoreCase("manager")) {
                            return;
                        }
                        break;
                    case "admin":
                        if (itemName.equalsIgnoreCase("owner")||itemName.equalsIgnoreCase("manager")||itemName.equalsIgnoreCase("admin")) {
                            return;
                        }
                        break;
                    case "developer":
                        if (itemName.equalsIgnoreCase("owner")||itemName.equalsIgnoreCase("manager")||itemName.equalsIgnoreCase("admin")||itemName.equalsIgnoreCase("developer")) {
                            return;
                        }
                        break;
                }
                Main.perms.playerRemoveGroup(null, target, group);
                Main.perms.playerAddGroup(null, target, itemName);
                p.sendMessage(Main.c("Setrank", "You set " + target.getName() + "'s rank to &a" + itemName + "&7."));
                p.closeInventory();
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "sync console bungee nmperms user " + target.getName() + " setgroup " + (itemName.equalsIgnoreCase(
                        "Player")?"default":itemName));
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void protect(InventoryMoveItemEvent e) {
        String name1 = ChatColor.stripColor(e.getDestination().getName());
        String name2 = ChatColor.stripColor(e.getInitiator().getName());
        if (name1.matches("Set .+'s rank") || name2.matches("Set .+'s rank")) {
            e.setCancelled(true);
        }
    }
}
