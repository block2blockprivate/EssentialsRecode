package net.raptorpvp.RaptorPvPCore.listeners;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryClickMainInv implements Listener {


    @EventHandler(priority = EventPriority.HIGH)
    public void invclick(InventoryClickEvent e) {
        String invname;
        try {
            invname = ChatColor.stripColor(e.getClickedInventory().getName());
        } catch (NullPointerException n) {
            return;
        }
        String name = ChatColor.stripColor(invname);
        if (name.equalsIgnoreCase("container.inventory")) {
            int slot = e.getSlot();
            if (slot > 8)
                return;
            ItemStack item = e.getClickedInventory().getItem(slot);
            if (item == null)
                return;
            String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
            if (itemName.equalsIgnoreCase("Staff/Test Servers")||itemName.equalsIgnoreCase("Server Navigation")) {
                if (e.getWhoClicked().getGameMode() != GameMode.CREATIVE) {
                    e.setCancelled(true);
                }
            }
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void protect(InventoryMoveItemEvent e) {
        String name1;
        String name2;

        try {
            name1 = ChatColor.stripColor(e.getDestination().getName());
            name2 = ChatColor.stripColor(e.getInitiator().getName());
        } catch (NullPointerException n) {
            return;
        }

        if (name1.equalsIgnoreCase("container.inventory") || name2.equalsIgnoreCase("container.inventory")) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void invdrop(PlayerDropItemEvent e){
        String name;
        try {
            name = ChatColor.stripColor(e.getItemDrop().getItemStack().getItemMeta().getDisplayName());
        } catch (NullPointerException n) {
            return;
        }
        if (name.equalsIgnoreCase("Staff/Test Servers")||name.equalsIgnoreCase("Server Navigation")||name.equalsIgnoreCase("Preferences")) {
            e.setCancelled(true);
        }
    }
}
