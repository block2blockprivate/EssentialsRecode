package net.raptorpvp.RaptorPvPCore.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InvClickServerNav implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onClickServerNav(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player p = (Player) e.getWhoClicked();
        String invname;
        try {
            invname = ChatColor.stripColor(e.getClickedInventory().getName());
        } catch (NullPointerException n) {
            return;
        }
        String name = ChatColor.stripColor(invname);
        if (name.equalsIgnoreCase("Server Navigation")) {
            e.setCancelled(true);
            int slot = e.getSlot();
            if (slot < 9)
                return;
            ItemStack item = e.getInventory().getItem(slot);
            if (item == null)
                return;
            String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
            try {
                switch (itemName.toLowerCase()) {
                    case "zombie survival":
                        sendToServer("ZombieSurvival", ((Player) e.getWhoClicked()).getPlayer());
                        ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.NOTE_PLING,100,1);
                        e.getWhoClicked().closeInventory();
                        break;
                    case "kitpvp: evolved alpha":
                        sendToServer("KitPvP", ((Player) e.getWhoClicked()).getPlayer());
                        ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.NOTE_PLING,100,1);
                        e.getWhoClicked().closeInventory();
                        break;
                    default:
                        break;
                }
            } catch (Exception ex) {
                return;
            }
        } else {
            return;
        }

    }

    private static void sendToServer(String server, Player player) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(Main.get(), "BungeeCord", out.toByteArray());
    }

}
