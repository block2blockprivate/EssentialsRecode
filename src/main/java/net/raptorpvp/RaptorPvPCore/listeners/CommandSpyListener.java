package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.commands.CommandCommandSpy;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandSpyListener implements Listener {


    @EventHandler
    public void command(PlayerCommandPreprocessEvent e) {
        for (Player p : CommandCommandSpy.list) {
            if (p==e.getPlayer()) continue;
            try {
                p.sendMessage(Main.c("CommandSpy", e.getPlayer().getName() + ": &a" + e.getMessage()));
            } catch (Throwable t) {
            }
        }
    }
}
