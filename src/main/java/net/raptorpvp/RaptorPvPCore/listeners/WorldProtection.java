package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class WorldProtection implements Listener {

    private Main plugin = Main.get();

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            if (e.getPlayer().getGameMode() != GameMode.CREATIVE) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            e.setFoodLevel(30);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            if (e.getEntity() instanceof Player) {
                e.setCancelled(true);
                e.setDamage(0);
            }
        }
    }

    @EventHandler
    public void onArtDamage(EntityDamageEvent e) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            if (e.getEntity() instanceof Player) {
                e.setCancelled(true);
                e.setDamage(0);
            }
        }
    }



}
