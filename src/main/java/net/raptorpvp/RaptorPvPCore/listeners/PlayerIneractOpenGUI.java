package net.raptorpvp.RaptorPvPCore.listeners;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.commands.CommandPrefs;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class PlayerIneractOpenGUI implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onItemClick(PlayerInteractEvent e){
        String itemName;
        try {
            itemName = ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName());
        } catch (NullPointerException n) {
            return;
        }
        try {
            if (itemName.equalsIgnoreCase("Staff/Test Servers")) {
                openStaffGUI(e.getPlayer());
            } else if (itemName.equalsIgnoreCase("Server Navigation")) {
                openNavGUI(e.getPlayer());
            } else if (itemName.equalsIgnoreCase("Preferences")) {
                CommandPrefs.openGUI(e.getPlayer());
            } else {
                return;
            }
        } catch (NullPointerException e2) {
            return;
        }
    }

    public void openNavGUI(Player p) {
        Inventory inv = Bukkit.createInventory(null, 27, net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', "&2&lServer Navigation"));
        i(inv, 11, Material.DIAMOND_SWORD,"&2&lKitPvP: Evolved &c&lALPHA", 1, ";&7Survive as long as you can;&7while fighting off other players.;&7Choose your kit and move wisely,;&7as any move could be your last...;;&aClick to connect!",(short)0,false);
        i(inv, 15, Material.SKULL_ITEM, "&2&lZombie Survival", 1, ";&7Survival as long as you can;&7during a zombie apocalypse.;&7Adventure solo or with a group;&7of friends and see who can survive;&7the longest.;;&6&lOptional Resource Pack Available.;&r&7Make sure server resource packs;&7are enabled.;;&aClick to connect!",(short)2, false);
        p.openInventory(inv);

    }

    public void openStaffGUI(Player p) {
        Inventory inv = Bukkit.createInventory(null, 54, net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', "&2&lStaff/Test Servers"));
        i(inv, 4, Material.COMMAND, "&c&lDev Servers",1, ";&7These servers are where;&7Developers test and work;&7on exciting new games and;&7fun updates! Please do;&7not join these servers;&7unless there is a staff;&7testing session in progress.",(short)0,false);
        i(inv, 10, Material.BAKED_POTATO, "&6&lDev-1",1, ";&7Connect to the Dev-1 Server.;;&7Test Active: &aHotPotato;&7Dev: &aBlock2Block;&aClick to connect!",(short)0,false);
        i(inv, 13, Material.DIAMOND_SWORD, "&6&lDev-2",1, ";&7Connect to the Dev-2 Server.;;&7Test Active: &aNone;&7Dev: &aNone;&aClick to connect!",(short)0,false);
        i(inv, 16, Material.ENCHANTED_BOOK, "&6&lDev-3",1, ";&7Connect to the Dev-3 Server.;;&7Test Active: &aHub;&7Dev: &aHarrSon;&aClick to connect!",(short)0,false);
        i(inv, 31, Material.EMERALD, "&a&lStaff Servers",1, ";&7These servers are staff only;&7servers where staff can hang;&7out. Some servers are for;&7certain ranks, but should;&7mostly be for all staff.",(short)0,false);
        i(inv, 38, Material.REDSTONE_BLOCK, "&6&lStaff",1, ";&7Connect to the Staff server.;;&7Status: &4Coming Soon",(short)0,false);
        i(inv, 42, Material.WOOD_AXE, "&6&lBuild",1, ";&7Connect to the Build server.;;&7Status: &aOnline;&7Required Rank: &9Builder;&aClick to connect!",(short)0,false);
        p.openInventory(inv);
    }

    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing, String skullName) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Main.cr(name));
        if (lore != null) {
            im.setLore(Arrays.asList(Main.c(null, lore).split(";")));
        }
        if (glowing) {
            im.addEnchant(Enchantment.DURABILITY, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        is.setItemMeta(im);
        inv.setItem(slot, is);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing) {
        i(inv, slot, type, name, amount, lore, data, glowing, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data) {
        i(inv, slot, type, name, amount, lore, data, false);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore) {
        i(inv, slot, type, name, amount, lore, (short)0);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount) {
        i(inv, slot, type, name, amount, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name) {
        i(inv, slot, type, name, 1);
    }
    private static void i(Inventory inv, int slot, Material type) {
        i(inv, slot, type, "");
    }

}
