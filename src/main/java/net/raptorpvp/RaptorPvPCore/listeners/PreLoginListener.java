package net.raptorpvp.RaptorPvPCore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.StringTokenizer;

public class PreLoginListener implements Listener {

    @EventHandler
    public void onPreLogin(AsyncPlayerPreLoginEvent e) {

        StringTokenizer tok = new StringTokenizer((e.getAddress().toString().replace("/","").split(":"))[0], ".");

        if (tok.countTokens() != 4) {
            Bukkit.getLogger().info("Using offline proxy, denying access to server.");
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER,ChatColor.translateAlternateColorCodes('&', "&2Server Management>> &7You must connect to via the RaptorPvP proxy. You are not permitted to use another proxy to connect to the servers."));

        }
        if (!(e.getAddress().toString().replace("/","").split(":"))[0].matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}")) {
            Bukkit.getLogger().info("Using offline proxy, denying access to server.");
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER,ChatColor.translateAlternateColorCodes('&', "&2Server Management>> &7You must connect to via the RaptorPvP proxy. You are not permitted to use another proxy to connect to the servers."));
        }
        Bukkit.getLogger().info("Player's IP: " + (e.getAddress().toString().replace("/","").split(":"))[0]);
        if ((e.getAddress().toString().replace("/","").split(":"))[0].contains("127.0.0")||(e.getAddress().toString().replace("/","").split(":"))[0].contains("NOPROXYFORME")) {
            Bukkit.getLogger().info("Using offline proxy, denying access to server.");
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER,ChatColor.translateAlternateColorCodes('&', "&2Server Management>> &7You must connect to via the RaptorPvP proxy. You are not permitted to use another proxy to connect to the servers."));
        }

    }

}
