package net.raptorpvp.RaptorPvPCore.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MySQLManager {

    public static MySQLManager i;

    private static MySQL db;

    public MySQLManager() {
        i = this;
    }

    public void setup() throws SQLException, ClassNotFoundException {
        db = new MySQL("", "3306","SimplyBrandon1_Ignore", "SimplyBrandon1", "");
        db.openConnection();
    }

    public static boolean isIgnoredBy(String uuid, String uuid2) {
        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM `" + uuid + "`");
            List<String> uuids = new ArrayList<>();
            int i = 1;
            while (set.next()) {
                uuids.add(set.getString(i));
            }
            if (uuids.contains(uuid2)) {
                statement.close();
                return true;
            }
            return false;
        } catch (SQLException t) {
            t.printStackTrace();
            return false;
        }
    }

    public static void createTable(String uuid) {
        try {
            Statement statement = db.getConnection().createStatement();
            boolean set = statement.execute("CREATE TABLE IF NOT EXISTS `" + uuid + "` (`uuid` varchar(36))");
        } catch (SQLException t) {
            t.printStackTrace();
            return;
        }
    }

    public static List<String> getIgnored(String uuid) {
        List<String> list = new ArrayList<>();
        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM `" + uuid + "`");
            int i = 1;
            while (set.next()) {
                list.add(set.getString(i));
            }
        } catch (SQLException t) {
            t.printStackTrace();
        } finally {
            return list;
        }
    }
}
