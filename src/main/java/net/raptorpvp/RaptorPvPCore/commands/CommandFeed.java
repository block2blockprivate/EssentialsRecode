package net.raptorpvp.RaptorPvPCore.commands;

import java.util.List;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public final class CommandFeed implements CommandExecutor{
	@SuppressWarnings("unused")
	private Main plugin = Main.get();


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("feed")||cmd.getName().equalsIgnoreCase("food")||cmd.getName().equalsIgnoreCase("hunger")) {
			Player player = (Player) sender;
			if(sender instanceof ConsoleCommandSender){
	  			sender.sendMessage("This command can only be executed in-game.");
	  			return false;
	  		}
			if(player.hasPermission("rank.admin")){
				if(args.length == 1){
	  				final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
	  				if(matchedPlayers.size() != 0) {
	  					Player pm = matchedPlayers.get(0);
	  					if(pm == player) {
	  		  				player.setFoodLevel(30);
	  		  				player.sendMessage(ChatColor.DARK_GREEN + "Feed>> " + ChatColor.GRAY + "Your hunger has been restored.");
	  		  				return false;
	  					}
	  					pm.setFoodLevel(30);
	  					player.sendMessage(ChatColor.DARK_GREEN + "Feed>> " + ChatColor.GRAY + pm.getName() + "'s hunger has been restored.");
	  					pm.sendMessage(ChatColor.DARK_GREEN + "Feed>> " + ChatColor.GRAY + "Your hunger has been restored.");
	  					return false;
	  				} else {
			  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY +  "That player is not online.");
		  			}
	  			} else if (args.length == 0) {
	  				player.setFoodLevel(30);
	  				player.sendMessage(ChatColor.DARK_GREEN + "Feed>> " + ChatColor.GRAY + "Your hunger has been restored.");
	  				return false;
	  			} else {
	  				player.sendMessage(ChatColor.DARK_GREEN + "Feed>> " + ChatColor.GRAY + "Invalid Syntax.");
				}
			} else {
				player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
			}
		}
		return false;
	}

}
