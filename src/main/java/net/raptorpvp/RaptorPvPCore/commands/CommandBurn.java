package net.raptorpvp.RaptorPvPCore.commands;

import java.util.List;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public final class CommandBurn implements CommandExecutor{
	@SuppressWarnings("unused")
	private Main plugin = Main.get();


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("burn")||cmd.getName().equalsIgnoreCase("fire")) {
			if(sender instanceof ConsoleCommandSender){
	  			sender.sendMessage("This command can only be executed in-game.");
	  			return false;
	  		}
			Player player = (Player) sender;
			if(player.hasPermission("rank.admin")) {
				if(args.length == 2){
					try {
					@SuppressWarnings("unused")
					Integer time = Integer.parseInt(args[1]);
					} catch (NumberFormatException nfe) {
						player.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/burn [Player] [Time in seconds]");
						return false;
					}
					Integer time = Integer.parseInt(args[1]);
					time = time * 20;
					if(args[0].equalsIgnoreCase("all")){
						for(Player i : Bukkit.getOnlinePlayers()){
							i.setFireTicks(time);
							time = time / 20;
							i.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "You have been set on fire for " + ChatColor.GREEN + time + ChatColor.GRAY + " seconds.");
						}
						return false;
					}
	  				final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
	  				if(matchedPlayers.size() != 0) {
	  					Player pm = matchedPlayers.get(0);
	  					if(pm == player) {
							player.setFireTicks(time);
							time = time / 20;
							player.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "You have been set on fire for " + ChatColor.GREEN + time + ChatColor.GRAY + " seconds.");
							return false;
	  					}
	  					pm.setFireTicks(time);
						time = time / 20;
						pm.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "You have been set on fire for " + ChatColor.GREEN + time + ChatColor.GRAY + " seconds.");
						player.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "You have set " + pm.getName() + " on fire for " + ChatColor.GREEN + time + ChatColor.GRAY + " seconds.");
						return false;
	  				} else {
			  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY +  "That player is not online.");
		  			}
				} else {
					player.sendMessage(ChatColor.DARK_GREEN + "Burn>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/burn <Player> <Time in seconds>");
				}
			} else {
				player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
			}
		}
		return false;
	}
}
