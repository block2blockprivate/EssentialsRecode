package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandChat implements CommandExecutor {

    private static boolean silenced = false;
    private static int slow = 0;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("rank.admin")) {
            if (args.length == 0) {
                silenced = !silenced;
                Main.broadcast("ChatSlow", "The chat has been " + (silenced?"&7silenced for &aPermanent&7.":"&aenabled&7."));
            } else {
                String a = args[0];
                if (a.matches("[0-9]+")) {
                    int b = Integer.parseInt(a);
                    if (b > 1000) {
                        sender.sendMessage(Main.c("Time", "That number is too large."));
                    } else {
                        silenced = false;
                        slow = Integer.parseInt(a);
                        Main.broadcast("ChatSlow", (slow!=0?("A chat slow has been &aenabled &7set to &a"+a+"&7 seconds."):("The chat slow has been &cdisabled&7.")));
                    }
                } else {
                    sender.sendMessage(Main.c("ChatSlow", "That is not a valid number."));
                }
            }
        } else {
            sender.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
        }
        return true;
    }

    public static boolean isSilenced() {
        return silenced;
    }

    public static int getSlow() {
        return slow;
    }
}
