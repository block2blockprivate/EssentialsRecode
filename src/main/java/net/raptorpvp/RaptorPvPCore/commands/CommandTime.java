package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Author: JacobRuby
 */

public class CommandTime implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender.hasPermission("rank.admin")) {
            String a = args[0];
            if (a.matches("[0-9]+")) {
                int b = Integer.parseInt(a);
                if (b > 24000) {
                    sender.sendMessage(Main.c("Time", "That number is too large."));
                } else {
                    Bukkit.getWorlds().get(0).setTime(b);
                    sender.sendMessage(Main.c("Time", "Time set to " + a));
                }
            }
            int time = 0;
            switch (a.toLowerCase()) {
                case "day": time = 1000; break;
                case "noon": time = 6000; break;
                case "night": time = 13000; break;
                case "midnight": time = 18000; break;
                default: sender.sendMessage(Main.c("Time", "That is not a valid number.")); return true;
            }
            Bukkit.getWorlds().get(0).setTime(time);
            sender.sendMessage(Main.c("Time", "Time set to " + time));
        } else {
            sender.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
        }
        return true;
    }
}
