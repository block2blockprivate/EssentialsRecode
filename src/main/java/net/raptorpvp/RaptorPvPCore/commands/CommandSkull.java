package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class CommandSkull implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("rank.admin")) {
                if (args.length == 1) {
                    ItemStack i = new ItemStack(Material.SKULL_ITEM,1,(short)3);
                    SkullMeta s = (SkullMeta) i.getItemMeta();
                    s.setOwner(args[0]);
                    ItemMeta im = s;
                    i.setItemMeta(im);
                    player.getInventory().addItem(i);

                } else {
                    player.sendMessage(Main.c("Skull", "Invalid syntax. Correct syntax: &a/skull <username>"));
                }
            } else {
                player.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("Skull","This command cannot be executed from console."));
        }
        return false;
    }
}
