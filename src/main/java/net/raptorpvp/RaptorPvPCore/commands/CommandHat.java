package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandHat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("rank.admin")) {
                ItemStack hand = p.getItemInHand();
                if (hand == null) {
                    p.sendMessage(Main.c("Hat", "You must be holding an item to use this command."));
                } else {
                    ItemStack head = p.getEquipment().getHelmet();
                    p.getEquipment().setHelmet(hand);
                    p.setItemInHand(head);
                    p.sendMessage(Main.c("Hat", "Hat equipped."));
                }
            } else {
                p.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("Hat", "You can only use this command as a player."));
        }
        return true;
    }
}
