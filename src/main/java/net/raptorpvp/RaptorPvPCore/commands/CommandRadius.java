package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class CommandRadius implements CommandExecutor {

    private Main m = Main.get();

    public static Map<Player, Integer> map = new HashMap<>();

    public static int getRadius(Player p) {
        return map.getOrDefault(p, 0);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (m.getConfig().getBoolean("settings.hubmode")) {
            if (command.getName().equalsIgnoreCase("radius")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    if (p.hasPermission("rank.admin")) {
                        if (args.length == 0) {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Radius>>&7 Invalid Syntax. Correct" +
                                    " Syntax:&a /radius <Amount>"));
                        } else {
                            if (args[0].matches("[0-9]+")) {
                                int i = Integer.parseInt(args[0]);
                                if (i > 50) {
                                    p.sendMessage(Main.c("Radius", "&cThat radius is too large! The max radius is 50."));
                                } else if (i == 0) {
                                    map.remove(p);
                                    p.sendMessage(Main.c("Radius", "You &cdisabled &7your forcefield."));
                                } else {
                                    map.put(p, i);
                                    p.sendMessage(Main.c("Radius", "You &aenabled &7a forcefield radius of &a" + args[0] + "&7 blocks."));
                                }
                            } else p.sendMessage(Main.c("Radius", "&cThat is not a valid number!"));
                        }
                    } else {
                        p.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
                    }
                } else {
                    sender.sendMessage(Main.c("Radius", "This command can only be used as a Player."));
                }
            }
            if (command.getName().equalsIgnoreCase("forcefield")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    if (p.hasPermission("rank.Youtuber")) {
                        if (map.get(p) != null) {
                            map.remove(p);
                            p.sendMessage(Main.c("Forcefield", "You have &cdisabled &7forcefield."));
                        } else {
                            map.put(p, 4);
                            p.sendMessage(Main.c("Forcefield", "You have &aenabled &7forcefield."));
                        }
                    } else {
                        p.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
                    }
                } else {
                    sender.sendMessage("This command can only be used as a Player.");
                }
            }
            return true;
        } else {
            sender.sendMessage(Main.c("Radius","That command is disabled!"));
            return false;
        }
    }
}
