package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Author: JacobRuby
 */

public class CommandClear implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equals("clear")) {
            if (sender.hasPermission("rank.admin")) {
                if (args.length == 0) {
                    if (sender instanceof Player) {
                        ((Player) sender).getInventory().clear();
                        ((Player) sender).getEquipment().clear();
                        sender.sendMessage(Main.c("Clear", "You cleared your inventory."));
                    } else {
                        sender.sendMessage(Main.c("Clear", "You can only clear your inventory as a player."));
                    }
                } else {
                    final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                    if (matchedPlayers.size() != 0) {
                        Player t = matchedPlayers.get(0);
                        t.getInventory().clear();
                        t.getEquipment().clear();
                        t.sendMessage(Main.c("Clear", "Your inventory was cleared."));
                        sender.sendMessage(Main.c("Clear", "Cleared &a" + t.getName() + "&7's inventory."));
                    } else {
                        sender.sendMessage(Main.c("Clear", "That player is not online."));
                    }
                }
            } else {
                sender.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
            }
        }
        return true;
    }
}
