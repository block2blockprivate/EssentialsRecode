package net.raptorpvp.RaptorPvPCore.commands;

import java.util.List;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public final class CommandFly implements CommandExecutor {
    @SuppressWarnings("unused")
    private Main plugin = Main.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (plugin.getConfig().getBoolean("settings.hubmode")) {
            if (cmd.getName().equalsIgnoreCase("fly")) {
                if (sender instanceof ConsoleCommandSender) {
                    sender.sendMessage("This command can only be executed in-game.");
                    return false;
                }
                Player player = (Player) sender;
                if (player.hasPermission("rank.immortal")) {
                    if (args.length == 0) {
                        player.setAllowFlight(!player.getAllowFlight());
                        player.sendMessage(Main.c("Fly", player.getName() + "&7's Fly: " + (player.getAllowFlight() ? "&aTrue" : "&cFalse")));
                    } else if (args.length == 1) {
                        final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                        if (matchedPlayers.size() != 0) {
                            Player pm = matchedPlayers.get(0);
                            if (pm == player) {
                                player.setAllowFlight(!player.getAllowFlight());
                                player.sendMessage(Main.c("Fly", player.getName() + "&7's Fly: " + (player.getAllowFlight() ? "&aTrue" : "&cFalse")));
                                return false;
                            }
                            player.setAllowFlight(!pm.getAllowFlight());
                            player.sendMessage(Main.c("Fly", pm.getName() + "&7's Fly: " + (player.getAllowFlight() ? "&aTrue" : "&cFalse")));
                            pm.sendMessage(Main.c("Fly", pm.getName() + "&7's Fly: " + (player.getAllowFlight() ? "&aTrue" : "&cFalse")));
                        } else {
                            player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + "That player is not online.");
                        }
                    } else {
                        player.sendMessage(ChatColor.DARK_GREEN + "Fly>> " + ChatColor.GRAY + "Invalid Syntax.");
                    }
                } else {
                    player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
                }
            }
            return false;
        } else {
            sender.sendMessage(Main.c("Fly","That command is disabled!"));
            return false;
        }
    }
}

