package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandBc implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("rank.mod")) {
            if (args.length == 0) {
                sender.sendMessage(Main.c("Broadcast","&7Incorrect usage! Correct usage: &a/bc <message>"));
                return true;
            }
            String message = "";
            for (String s : args) {
                message += s + " ";
            }
            message = message.trim();
            Main.broadcast(null, "&3&l" + sender.getName() + "&b " + message);
        } else {
            sender.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
        }
        return true;
    }
}
