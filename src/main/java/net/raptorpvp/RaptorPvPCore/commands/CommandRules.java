package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandRules implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(Main.c("Rules","RaptorPvP rules can be found here: http://forums.raptorpvp.net/index.php?threads/raptorpvp-server-rules.70/"));
        return true;
    }
}
