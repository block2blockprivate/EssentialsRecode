package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class CommandLightning implements CommandExecutor{

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("rank.admin")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("all")) {
                        World world = p.getWorld();
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            world.strikeLightningEffect(target.getLocation());
                            target.sendMessage(Main.c("Lightning", "You have been smitten!"));
                        }
                        p.sendMessage(Main.c("Lightning","You have smitten everyone!"));
                        return false;
                    }
                    World world = p.getWorld();
                    final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                    if (matchedPlayers.size() != 0) {
                        world.strikeLightningEffect(matchedPlayers.get(0).getLocation());
                        matchedPlayers.get(0).sendMessage(Main.c("Lightning", "You have been smitten!"));
                        p.sendMessage(Main.c("Lightning","You have smitten &a" + matchedPlayers.get(0).getName() + "&7."));
                    } else {
                        p.sendMessage(Main.c("Lightning", "That player could not be found."));
                    }
                } else {
                    p.sendMessage(Main.c("Lightning","Incorrect syntax. Correct syntax: &a/lightning <player>"));
                }
            } else {
                sender.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("Lightning","This command can only be executed from in-game."));
        }
        return false;
    }
}
