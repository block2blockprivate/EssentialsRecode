package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.entities.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStaff implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 0) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                String group = Main.perms.getPrimaryGroup("hub", player);

                String message = "";
                for (String s : args) {
                    message += s + " ";
                }
                message = message.trim();
                Rank rank = Rank.getGroup(player);
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (p == player) continue;
                    if (Rank.getGroup(p).isStaff()) {
                        p.sendMessage(Main.c("Help",  "&" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " +
                                player.getName() + " &2» &a") + message);
                    }
                }

                player.sendMessage(Main.c("Help",  "&" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " +
                        player.getName() + " &2» &a") + message);
            } else {
                sender.sendMessage(Main.c("Help","You can only execute this command from in-game."));
            }

        } else {
            sender.sendMessage(Main.c("Help","You must provide a message."));
        }

        return false;
    }
}
