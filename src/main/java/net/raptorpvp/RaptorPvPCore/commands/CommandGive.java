package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Author: JacobRuby
 */


public class CommandGive implements CommandExecutor {

    private static Main m = Main.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        /*
         * Format: /give player:all item:data amount -nName -eEnchant
         */

        if (cmd.getName().equalsIgnoreCase("give")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (p.hasPermission("rank.admin")) {
                    if (args.length <= 1) {
                        p.sendMessage(Main.c("Item", "Invalid Syntax. Correct Syntax: &a/give <Player, all> <Item:Data> [Amount] " +
                                "&2[Additional variables: -n<Name>, -e<Enchantment:Level>]"));
                    } else {

                        String var1 = args[1];
                        ItemStack item = null;
                        boolean biggerThanStack = false;
                        int var4 = -1;
                        int var5 = -1;
                        int var6 = 1;
                        String materials = "";
                        for (Material m : Material.values()) {
                            materials += m.name() + " ";
                        }
                        materials = materials.trim();
                        if (args.length > 2) {
                            if (args[2].matches("[1-9]|[1-9][0-9]{1,3}")) {
                                var6 = Integer.parseInt(args[2]);
                                biggerThanStack = var6 > 64;
                                if (biggerThanStack) {
                                    var4 = var6 % 64;
                                    var5 = var6 / 64;
                                }
                            } else {
                                p.sendMessage(Main.c("Item", "You must enter a valid number! (" + args[2] + ")\n&7The maximum is 9999."));
                                return true;
                            }
                        }
                        if (var1.toLowerCase().matches("[a-z_]{3,}:([1-9]||1[0-5])")) {
                            String var2 = var1.split(":")[0].toUpperCase();
                            if (materials.contains(var2)) {
                                item = new ItemStack(Material.valueOf(var2), 1, Short.parseShort(var1.split(":")[1]));
                            } else {
                                p.sendMessage(Main.c("Item", "That is not a valid material!"));
                                return true;
                            }
                        } else if (var1.toLowerCase().matches("[a-z_]{3,26}")) {
                            String var3 = var1.toUpperCase();
                            if (materials.contains(var3)) {
                                item = new ItemStack(Material.valueOf(var3), var6);
                            } else {
                                p.sendMessage(Main.c("Item", "That is not a valid material!"));
                                return true;
                            }
                        } else {
                            p.sendMessage(Main.c("Item", "This is not a valid material set!"));
                            return true;
                        }
                        ItemMeta itemMeta = item.getItemMeta();
                        for (int i = 3; i < args.length; i++) {
                            String var7 = args[i];
                            if (var7.matches("-[ne].+")) {
                                String var8 = var7.replaceFirst("-", "");
                                String enchants = "";
                                for (Enchantment e : Enchantment.values()) {
                                    enchants += e.getName() + " ";
                                }
                                enchants = enchants.trim();
                                switch (var8.charAt(0)) {
                                    case 'n': {
                                        String var9 = var8.replaceFirst("n", "").replaceAll("_", " ");
                                        itemMeta.setDisplayName(Main.c(null, "&f" + var9));
                                        break;
                                    }
                                    case 'e': {
                                        String var10 = var8.replaceFirst("e", "").toLowerCase();
                                        if (var10.matches("[a-z_]{4,24}:([1-9]|[1-9][0-9]{1,3})")) {
                                            String var11 = var10.split(":")[0].toLowerCase();
                                            switch (var11) {
                                                case "protection":
                                                    var11 = "damage_all";
                                                    break;
                                                case "fire_protection":
                                                    var11 = "protection_fire";
                                                    break;
                                                case "feather_falling":
                                                    var11 = "protection_fall";
                                                    break;
                                                case "blast_protection":
                                                    var11 = "protection_explosions";
                                                    break;
                                                case "projectile_protection":
                                                    var11 = "protection_projectile";
                                                    break;
                                                case "respiration":
                                                    var11 = "oxygen";
                                                    break;
                                                case "aqua_affinity":
                                                    var11 = "water_worker";
                                                    break;
                                                case "sharpness":
                                                    var11 = "damage_all";
                                                    break;
                                                case "smite":
                                                    var11 = "damage_undead";
                                                    break;
                                                case "bane":
                                                    var11 = "damage_arthropods";
                                                    break;
                                                case "looting":
                                                    var11 = "loot_bonus";
                                                    break;
                                                case "efficiency":
                                                    var11 = "dig_speed";
                                                    break;
                                                case "unbreaking":
                                                    var11 = "durability";
                                                    break;
                                                case "fortune":
                                                    var11 = "loot_bonus_blocks";
                                                    break;
                                                case "power":
                                                    var11 = "arrow_damage";
                                                    break;
                                                case "punch":
                                                    var11 = "arrow_knockback";
                                                    break;
                                                case "flame":
                                                    var11 = "arrow_fire";
                                                    break;
                                                case "infinity":
                                                    var11 = "arrow_infinite";
                                                    break;
                                                default:
                                            }
                                            if (enchants.contains(var11.toUpperCase())) {
                                                itemMeta.addEnchant(Enchantment.getByName(var11.toUpperCase()), Integer.parseInt(var10.split(":")[1]), true);
                                            } else {
                                                p.sendMessage(Main.c("Item", "That is not a valid enchantment, the valid types are as follows:\n&7" + enchants));
                                                continue;
                                            }
                                        } else {
                                            p.sendMessage(Main.c("Item", "That is not a valid enchantment! (" + var10.toUpperCase() + ")"));
                                            continue;
                                        }
                                    }
                                    default: {

                                    }
                                }
                            }
                        }
                        item.setItemMeta(itemMeta);
                        ItemStack itemStack = item.clone();
                        itemStack.setAmount(64);
                        String itemName = var1.toUpperCase().charAt(0) + var1.split(":")[0].toLowerCase().replaceFirst(String.valueOf(var1.toLowerCase().charAt(0)), "");
                        try {
                            final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                            boolean all = args[0].equalsIgnoreCase("all");
                            if (matchedPlayers.size() != 0 || all) {
                                Player t = null;
                                if (!all) t = matchedPlayers.get(0);
                                if (biggerThanStack) {
                                    for (int i = 0; i < var5; i++) {
                                        if (all) {
                                            Bukkit.getServer().getOnlinePlayers().forEach(player -> player.getInventory().addItem(itemStack));
                                        } else t.getInventory().addItem(itemStack);
                                    }
                                    itemStack.setAmount(var4);
                                    if (var4 != 0) {
                                        if (all) {
                                            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                                                player.getInventory().addItem(itemStack);
                                                player.sendMessage(Main.c("Item", "You have been given &a" + var6 + " &a" + itemName + "&7 from &a" + p.getName() + "&7."));
                                            }
                                            p.sendMessage(Main.c("Item", "You have given &a" + var6 + " &a" + itemName + "&7 to &aEveryone&7."));
                                        } else {
                                            t.getInventory().addItem(itemStack);
                                            t.sendMessage(Main.c("Item", "You have been given &a" + var6 + " &a" + itemName + "&7 from &a" + p.getName() + "&7."));
                                            p.sendMessage(Main.c("Item", "You have given &a" + var6 + " &a" + itemName + "&7 to &a" + t.getName() + "&7."));
                                        }
                                    }
                                    return true;
                                } else {
                                    for (int i = 0; i < var6; i++) {
                                        if (all) {
                                            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                                                player.getInventory().addItem(item);
                                            }
                                        } else {
                                            t.getInventory().addItem(item);
                                        }
                                    }
                                    if (all) {
                                        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                                            player.sendMessage(Main.c("Item", "You have been given &a" + var6 + " &a" + itemName + "&7 from &a" + p.getName() + "&7."));
                                        }
                                        p.sendMessage(Main.c("Item", "You have given &a" + var6 + " &a" + itemName + "&7 to &aEveryone&7."));
                                    } else {
                                        t.sendMessage(Main.c("Item", "You have been given &a" + var6 + " &a" + itemName + "&7 from &a" + p.getName() + "&7."));
                                        p.sendMessage(Main.c("Item", "You have given &a" + var6 + " &a" + itemName + "&7 to &a" + t.getName() + "&7."));
                                    }
                                    return true;
                                }
                            } else {
                                p.sendMessage(Main.c("Item", "That player is not online."));
                            }
                        } catch (Exception e) {
                            p.sendMessage(e.getMessage());
                        }
                    }
                } else {
                    p.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
                }
            }
        }


        return true;
    }
}
