package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.entities.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandStaffMessage implements CommandExecutor {

    private static Map<Player, String> lastSent = new HashMap<>();

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("rank.staff")) {
                if (args.length > 1) {
                    final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                    if (matchedPlayers.size() != 0) {
                        if (matchedPlayers.get(0) != p) {
                            String group = Main.perms.getPrimaryGroup("hub", p);
                            Rank rank = Rank.getGroup(p);
                            Player p2 = matchedPlayers.get(0);
                            String group2 = Main.perms.getPrimaryGroup("hub", p2);
                            Rank rank2 = Rank.getGroup(p2);

                            String message = "";
                            for (String s : args) {
                                if (s.equals(args[0])) continue;
                                message += s + " ";
                            }
                            message = message.trim();
                            for (Player target : Bukkit.getOnlinePlayers()) {
                                if (target == p) continue;
                                if (target == p2) continue;
                                if (Rank.getGroup(target).isStaff()) {
                                    target.sendMessage(Main.c("Help", "&" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " +
                                            p.getName() + " &7-> &" + String.valueOf(rank2.getDisplayName().charAt(1)) + rank2.getName() + " " + p2.getName() + " &2» &a") + message);
                                }
                            }
                            p.sendMessage(Main.c("Help", "To: &" + String.valueOf(rank2.getDisplayName().charAt(1)) + rank2.getName() + " " + p2.getName() + " &2» &a") + message);
                            p2.sendMessage(Main.c("Help", "From: &" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " + p.getName() + " &2» &a") + message);
                            if (getMap().containsKey(p)) {
                                getMap().remove(p);
                            }
                            getMap().put(p,p2.getName());

                        } else {
                            p.sendMessage(Main.c("Help", "You cannot message yourself!"));
                        }
                    } else {
                        p.sendMessage(Main.c("Help", "That player is not online."));
                    }
                } else {
                    p.sendMessage(Main.c("Help","Incorrect syntax. Correct syntax: &a/staffmessage <player> <message>"));
                }
            } else {
                p.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("Help","This command can only be executed from in-game."));
        }
        return false;
    }

    public static String getLast(Player p) {
        return lastSent.get(p);
    }

    public static Map<Player, String> getMap() {
        return lastSent;
    }

}
