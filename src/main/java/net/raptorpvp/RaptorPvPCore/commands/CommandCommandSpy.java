package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandCommandSpy implements CommandExecutor {

    public static List<Player> list = new ArrayList<>();

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("rank.admin")) {
                if (list.contains(p)) {
                    list.remove(p);
                    p.sendMessage(Main.c("CommandSpy", "You have &cdisabled&7 Command Spy."));
                } else {
                    list.add(p);
                    p.sendMessage(Main.c("CommandSpy","You have &aenabled &7Command Spy."));
                }
            } else {
                sender.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("CommandSpy","This command can only be executed from in-game."));
        }
        return false;
    }

}
