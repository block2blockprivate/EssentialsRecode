package net.raptorpvp.RaptorPvPCore.commands;

import net.md_5.bungee.api.ChatColor;
import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

import static org.bukkit.Material.*;

public class CommandSetrank implements CommandExecutor {

    private static Main m = Main.get();

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("rank.admin")) {
                if (args.length == 1) {
                    OfflinePlayer target = Main.getMainConfig().getOfflinePlayer("players." + args[0].toLowerCase());
                    if (target == null) {
                        player.sendMessage(Main.c("Setrank", "That player does not exist."));
                    } else {
                        if (player.getName().equalsIgnoreCase(args[0])){
                            player.sendMessage(Main.c("Setrank","You cannot set your own rank."));
                        } else {
                            String group;
                            try {
                                group = Main.perms.getPrimaryGroup("hub", target);
                            } catch (Exception e) {
                                group = "Player";
                            }
                            if (!group.toLowerCase().equals("owner")||Main.perms.getPrimaryGroup("hub",(Player)sender).toLowerCase().equals("owner")) {
                                openGUI(player, target);
                            } else {
                                sender.sendMessage(Main.c("Setrank","You are not allowed to set an Owner's rank."));
                            }
                        }
                    }
                } else {
                    player.sendMessage(Main.c("Setrank", "Invalid syntax. Correct syntax: &a/setrank <player>"));
                }
            } else {
                player.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage("This command can only be executed in-game.");
        }
        return true;
    }

    public void openGUI(Player player, OfflinePlayer target) {
        String group;
        Inventory inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&2&lSet " + target.getName() + "'s rank"));
        String playergroup = Main.perms.getPrimaryGroup("hub",player);
        try {
            group = Main.perms.getPrimaryGroup("hub", target);
        } catch (Exception e) {
            group = "Player";
        }
        i(inv, 4, SKULL_ITEM, "&6&l" + target.getName(), 1, "Current Rank: &a" + group, (short)3, false, target.getName());
        i(inv, 10, STAINED_GLASS_PANE, "&f&lPlayer", 1, "Set " + target.getName() + "'s rank to Player", (short)0, group.equals("default")||group.equals("Player"));
        i(inv, 12, STAINED_GLASS_PANE, "&c&lYouTuber", 1, "Set " + target.getName() + "'s rank to YouTuber", (short)0, group.equals("YouTuber"));
        if (playergroup.equalsIgnoreCase("Developer")) {
            i(inv, 14, STAINED_GLASS_PANE, "&a&lDeveloper", 1, "&c&lYou are not allowed to set this rank!", (short)5, group.equals("Developer"));
        } else {
            i(inv, 14, STAINED_GLASS_PANE, "&a&lDeveloper", 1, "Set " + target.getName() + "'s rank to Developer", (short)5, group.equals("Developer"));
        }
        i(inv, 20, STAINED_GLASS_PANE, "&3&lSFE", 1, "Set " + target.getName() + "'s rank to SFE", (short)0, group.equals("SFE"));
        if (playergroup.equalsIgnoreCase("Owner")) {
            i(inv, 16, STAINED_GLASS_PANE, "&4&lOwner", 1, "Set " + target.getName() + "'s rank to Owner", (short)14, group.equals("Owner"));
            i(inv, 34, STAINED_GLASS_PANE, "&4&lManager", 1, "Set " + target.getName() + "'s rank to Manager", (short)14, group.equals("Manager"));
        } else {
            i(inv, 16, STAINED_GLASS_PANE, "&4&lOwner", 1, "&c&lYou are not allowed to set this rank!", (short)14, group.equals("Owner"));
            i(inv, 34, STAINED_GLASS_PANE, "&4&lManager", 1, "&c&lYou are not allowed to set this rank!", (short)14, group.equals("Manager"));
        }
        i(inv, 46, STAINED_GLASS_PANE, "&d&lImmortal", 1, "Set " + target.getName() + "'s rank to Immortal", (short)2, group.equals("Immortal"));
        i(inv, 30, STAINED_GLASS_PANE, "&a&lHelper", 1, "Set " + target.getName() + "'s rank to Helper", (short)5, group.equals("Helper"));
        i(inv, 32, STAINED_GLASS_PANE, "&9&lBuilder", 1, "Set " + target.getName() + "'s rank to Builder", (short)3, group.equals("Builder"));
        if (!playergroup.equalsIgnoreCase("Admin")) {
            i(inv, 52, STAINED_GLASS_PANE, "&c&lAdmin", 1, "Set " + target.getName() + "'s rank to Admin", (short)6, group.equals("Admin"));
        } else {
            i(inv, 52, STAINED_GLASS_PANE, "&c&lAdmin", 1, "&c&lYou are not allowed to set this rank!", (short)6, group.equals("Admin"));
        }
        i(inv, 34, STAINED_GLASS_PANE, "&4&lManager", 1, "Set " + target.getName() + "'s rank to Manager", (short)14, group.equals("Manager"));
        i(inv, 28, STAINED_GLASS_PANE, "&b&lOverlord", 1, "Set " + target.getName() + "'s rank to Overlord", (short)3, group.equals("Overlord"));
        i(inv, 48, STAINED_GLASS_PANE, "&6&lModerator", 1, "Set " + target.getName() + "'s rank to Moderator", (short)1, group.equals("Moderator"));
        i(inv, 50, STAINED_GLASS_PANE, "&9&lArtist", 1, "Set " + target.getName() + "'s rank to Artist", (short)3, group.equals("Artist"));

        player.openInventory(inv);
        Bukkit.getServer().dispatchCommand(player, "");
    }

    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing, String skullName) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Main.c(null, name));
        if (lore != null) {
            im.setLore(Arrays.asList(Main.c(null, lore).split(",")));
        }
        if (glowing) {
            im.addEnchant(Enchantment.DURABILITY, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        is.setItemMeta(im);
        inv.setItem(slot, is);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing) {
        i(inv, slot, type, name, amount, lore, data, glowing, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data) {
        i(inv, slot, type, name, amount, lore, data, false);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore) {
        i(inv, slot, type, name, amount, lore, (short)0);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount) {
        i(inv, slot, type, name, amount, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name) {
        i(inv, slot, type, name, 1);
    }
    private static void i(Inventory inv, int slot, Material type) {
        i(inv, slot, type, "");
    }
}
