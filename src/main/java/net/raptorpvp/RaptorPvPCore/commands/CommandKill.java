package net.raptorpvp.RaptorPvPCore.commands;

import java.util.List;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public final class CommandKill implements CommandExecutor{
	@SuppressWarnings("unused")
	private Main plugin = Main.get();


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("kill")||cmd.getName().equalsIgnoreCase("killplayer")) {
			Player player = (Player) sender;
			if(sender instanceof ConsoleCommandSender){
	  			sender.sendMessage("This command can only be executed in-game.");
	  			return false;
	  		}
			if(player.hasPermission("rank.admin")){
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("all")){
						for(Player i : Bukkit.getOnlinePlayers()){
							i.setHealth(0);
							i.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + "You have been killed.");
						}
						return false;
					}
	  				final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
	  				if(matchedPlayers.size() != 0) {
	  					Player pm = matchedPlayers.get(0);
	  					if(pm == player) {
	  						player.setHealth(0);
	  						player.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + "You have commited suicide.");
	  						return false;
	  					}
	  					pm.setHealth(0);
	  					player.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + pm.getName() + " has been killed.");
	  					pm.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + "You have been killed.");
	  					return false;
	  				} else {
			  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY +  "That player is not online.");
		  			}
				} else if (args.length == 0) {
					player.setHealth(0);
					player.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + "You have commited suicide.");
					return false;
				} else {
					player.sendMessage(ChatColor.DARK_GREEN + "Kill>> " + ChatColor.GRAY + "Invalid Syntax.");
				}
			} else {
				player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
			}
		}
		return false;
	}
}
