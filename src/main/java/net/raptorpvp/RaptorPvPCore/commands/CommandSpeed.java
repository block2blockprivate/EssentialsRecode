package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.List;

public final class CommandSpeed  implements Listener, CommandExecutor {

    @SuppressWarnings("unused")
    private Main plugin = Main.get();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("speed")) {
            Player player = (Player) sender;
            if (sender instanceof ConsoleCommandSender) {
                sender.sendMessage("This command can only be executed in-game.");
                return false;
            }
            if (player.hasPermission("rank.admin")) {
                if (args.length == 1) {
                    try {
                        Integer speed = Integer.parseInt(args[0]);
                    } catch (NumberFormatException nfe) {
                        player.sendMessage(ChatColor.DARK_GREEN + "Speed>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/speed <speed> [player]");
                        return false;
                    }
                    Integer speed = Integer.parseInt(args[0]);
                    float speed2 = speed / 10;
                    if (speed > 10||speed < 1) {
                        player.sendMessage(Main.c("Speed", "That is not a valid speed."));
                        return false;
                    }
                    player.setFlySpeed((float)0.1);
                    player.setWalkSpeed((float)0.2);
                    player.sendMessage(Main.c("Speed", "Your fly and walk speed has been set to " + speed));
                } else if (args.length == 2) {
                    try {
                        Integer speed = Integer.parseInt(args[1]);
                    } catch (NumberFormatException nfe) {
                        player.sendMessage(ChatColor.DARK_GREEN + "Speed>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/speed <speed> [player]");
                        return false;
                    }
                    Integer speed = Integer.parseInt(args[1]);
                    if (speed > 10) {
                        player.sendMessage(Main.c("Speed", "You cannot set a speed that high."));
                        return false;
                    }
                    final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
                    if (matchedPlayers.size() != 0) {
                        Player pm = matchedPlayers.get(0);
                        if (pm == player) {
                            player.setFlySpeed(speed);
                            player.setWalkSpeed(speed);
                            player.sendMessage(Main.c("Speed", "Your fly and walk speed has been set to " + speed));
                            return false;
                        }
                        pm.setFlySpeed(speed);
                        pm.setWalkSpeed(speed);
                        player.sendMessage(Main.c("Speed", "You set &a" + pm.getName() + "'s fly and walk speed has been set to " + speed));
                        pm.sendMessage(Main.c("Speed", "Your fly and walk speed has been set to " + speed));
                        return false;
                    } else {
                        player.sendMessage(ChatColor.DARK_GREEN + "Heal>> " + ChatColor.GRAY + "Invalid Syntax.");
                    }
                }
            }
        }
        return false;
    }
}
