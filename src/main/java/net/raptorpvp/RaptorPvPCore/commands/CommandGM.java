package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public final class CommandGM implements CommandExecutor{
	@SuppressWarnings("unused")
	private Main plugin = Main.get();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	  	if (cmd.getName().equalsIgnoreCase("gm")||cmd.getName().equalsIgnoreCase("gamemode")) {
	  		Player player = (Player) sender;
	  		if(sender instanceof ConsoleCommandSender){
	  			sender.sendMessage("This command can only be executed in-game.");
	  			return false;
	  		}
	  		if(player.hasPermission("rank.admin")){
	  			if(args.length == 0){
			  		if(player.getGameMode() == GameMode.CREATIVE){
			  			player.setGameMode(GameMode.SURVIVAL);
			  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + player.getName() + "'s Gamemode: " + ChatColor.RED + "False");
			  		} else {
			  			player.setGameMode(GameMode.CREATIVE);
			  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + player.getName() + "'s Gamemode: " + ChatColor.GREEN + "True");
			  		}
	  			} else if(args.length == 1){
	  				final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
	  				if(matchedPlayers.size() != 0) {
	  					Player pm = matchedPlayers.get(0);
	  					if(pm == player) {
	  				  		if(player.getGameMode() == GameMode.CREATIVE){
	  				  			player.setGameMode(GameMode.SURVIVAL);
	  				  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + player.getName() + "'s Gamemode: " + ChatColor.RED + "False");
	  				  		} else {
	  				  			player.setGameMode(GameMode.CREATIVE);
	  				  			player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + player.getName() + "'s Gamemode: " + ChatColor.GREEN + "True");
	  				  		}
	  				  		return false;
	  					}
	  					if(pm.getGameMode() == GameMode.CREATIVE){
	  						pm.setGameMode(GameMode.SURVIVAL);
	  						player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + pm.getName() + "'s Gamemode: " + ChatColor.RED + "False");
	  						pm.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + pm.getName() + "'s Gamemode: " + ChatColor.RED + "False");
	  						return false;
	  					} else{
	  						pm.setGameMode(GameMode.CREATIVE);
	  						player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + pm.getName() + "'s Gamemode: " + ChatColor.GREEN + "True");
	  						pm.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + pm.getName() + "'s Gamemode: " + ChatColor.GREEN + "True");
	  						return false;
	  					}
	  				} else {
		  				player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY +  "That player is not online.");
	  				}
	  			} else{
	  				player.sendMessage(ChatColor.DARK_GREEN + "GM>> " + ChatColor.GRAY + "Invalid Syntax.");
	  			}
	  		} else {
				player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
			}
	  	}
		return false; 
	  }
}
