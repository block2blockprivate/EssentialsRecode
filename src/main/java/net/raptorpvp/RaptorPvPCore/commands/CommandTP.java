package net.raptorpvp.RaptorPvPCore.commands;

import java.util.List;

import net.raptorpvp.RaptorPvPCore.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public final class CommandTP  implements CommandExecutor{
	@SuppressWarnings("unused")
	private Main plugin = Main.get();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("tp")||cmd.getName().equalsIgnoreCase("teleport")||cmd.getName().equalsIgnoreCase("teleportplayer")) {
			Player player = (Player) sender;
			if (player.hasPermission("rank.mod")) {
				if(sender instanceof ConsoleCommandSender){
		  			sender.sendMessage("This command can only be executed in-game.");
		  			return false;
		  		}
				if(args.length == 2){
					final List<Player> matchedPlayers = Bukkit.getServer().matchPlayer(args[0]);
					final List<Player> matchedPlayers2 = Bukkit.getServer().matchPlayer(args[1]);
					if (args[0].equalsIgnoreCase("all")){
						Location l = matchedPlayers2.get(0).getLocation();
						for (Player i : Bukkit.getOnlinePlayers()){
							i.teleport(l);
							if (i != player) {
								i.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You have been teleported to " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + ".");
							}
						}
						matchedPlayers2.get(0).sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "Everyone was teleported to you.");
						if (matchedPlayers2.get(0) != player) {
							player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You teleported everyone to " + ChatColor.GREEN + matchedPlayers2.get(0).getName() + ChatColor.GRAY + ".");
						}
						return false;
					}
					if (matchedPlayers.size() != 0 && matchedPlayers2.size() != 0) {
						if (matchedPlayers2.get(0) != matchedPlayers.get(0)) {
							Location l2 = matchedPlayers2.get(0).getLocation();
							matchedPlayers.get(0).teleport(l2);
							matchedPlayers.get(0).sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You have been teleported to " + ChatColor.GREEN + matchedPlayers2.get(0).getName() + ChatColor.GRAY + ".");
							if (matchedPlayers2.get(0).getName() == sender.getName()) {
								sender.sendMessage(Main.c("TP","You have teleported &a" + matchedPlayers.get(0).getName() + "&7 to yourself."));
							} else {
								sender.sendMessage(Main.c("TP","You have teleported &a" + matchedPlayers.get(0).getName() + "&7 to &a" + matchedPlayers2.get(0).getName() + "&7."));
							}
						} else {
							player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You cannot teleport someome to themself.");
						}
					} else {
						player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/tp <Player> <Player>" + ChatColor.GRAY + ".");
						return false;
					}
				} else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("all")){
						Location l3 = player.getLocation();
						for (Player i : Bukkit.getOnlinePlayers()){
							i.teleport(l3);
							if (i != player) {
								i.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You have been teleported to " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + ".");
							}
						}
						player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You teleported everyone to yourself.");
						return false;
					}
					final List<Player> matchedPlayers3 = Bukkit.getServer().matchPlayer(args[0]);
					if (matchedPlayers3.size() != 0 ) {
						if (matchedPlayers3.get(0) != player) {
							Location l3 = matchedPlayers3.get(0).getLocation();
							player.teleport(l3);
							player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You teleported to " + ChatColor.GREEN + matchedPlayers3.get(0).getName() + ChatColor.GRAY + ".");
						} else {
							player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "You cannot teleport to yourself!");
						}
					}
				} else {
					player.sendMessage(ChatColor.DARK_GREEN + "TP>> " + ChatColor.GRAY + "Invalid Syntax. Correct Syntax: " + ChatColor.GREEN + "/tp <Player> <Player>" + ChatColor.GRAY + ".");
				}
			} else {
				player.sendMessage(Main.c("Command Manager", "&cSorry! That command is unknown!"));
			}
		}
		return false;
	}
}
