package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.entities.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStaffReply implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("rank.staff")) {
                if (args.length != 0) {
                    if (CommandStaffMessage.getMap().containsKey(p)) {
                        String group = Main.perms.getPrimaryGroup("hub", p);
                        Rank rank = Rank.getGroup(p);
                        Player p2 = Bukkit.getPlayer(CommandStaffMessage.getMap().get(p));
                        if (p2 == null) {
                            p.sendMessage(Main.c("Help","That player is no longer online."));
                            return false;
                        }
                        String group2 = Main.perms.getPrimaryGroup("hub", p2);
                        Rank rank2 = Rank.getGroup(p2);

                        String message = "";
                        for (String s : args) {
                            message += s + " ";
                        }
                        message = message.trim();
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            if (target == p) continue;
                            if (target == p2) continue;
                            if (Rank.getGroup(target).isStaff()) {
                                target.sendMessage(Main.c("Help", "&" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " +
                                        p.getName() + " &7-> &" + String.valueOf(rank2.getDisplayName().charAt(1)) + rank2.getName() + " " + p2.getName() + " &2» &a") + message);
                            }
                        }
                        p.sendMessage(Main.c("Help", "To: &" + String.valueOf(rank2.getDisplayName().charAt(1)) + rank2.getName() + " " + p2.getName() + " &2» &a") + message);
                        p2.sendMessage(Main.c("Help", "From: &" + String.valueOf(rank.getDisplayName().charAt(1)) + rank.getName() + " " + p.getName() + " &2» &a") + message);
                    } else {
                        p.sendMessage(Main.c("Help","You haven't messaged anyone yet!"));
                    }
                } else {
                    p.sendMessage(Main.c("Help", "Invalid syntax. Correct syntax: /staffreply <message>"));
                }
            } else {
                p.sendMessage(Main.c("Command Manager","&cSorry! That command is unknown!"));
            }
        } else {
            sender.sendMessage(Main.c("Help","This command can only be executed from in-game."));
        }
        return false;
    }
}
