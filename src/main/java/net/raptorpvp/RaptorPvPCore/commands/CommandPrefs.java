package net.raptorpvp.RaptorPvPCore.commands;

import net.raptorpvp.RaptorPvPCore.Main;
import net.raptorpvp.RaptorPvPCore.entities.Rank;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

import static org.bukkit.Material.*;

public class CommandPrefs implements CommandExecutor {

    static Main m = Main.get();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            openGUI((Player) sender);
        }
        return true;
    }

    public static void openGUI(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&2&lYour Preferences"));
        switch (Rank.getGroup(player).getName().toLowerCase()) {
            case "overlord":
                i(inv, 19, BOOK_AND_QUILL, "&2&lPlayer Chat", 1, "&7Disable your ability to see;&7other players chat.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".chat")) {
                    i(inv, 28, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 28, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 25, NETHER_STAR, "&2&lPlayer Visibility", 1, "&7Disable your ability to see;&7other player's.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".visibility")) {
                    i(inv, 34, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 34, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 22, FEATHER, "&2&lSpeed", 1, "&7Speed around the lobby!");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".speed")) {
                    i(inv, 31, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 31, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                break;
            case "immortal":
                i(inv, 19, BOOK_AND_QUILL, "&2&lPlayer Chat", 1, "&7Disable your ability to see;&7other player's chat.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".chat")) {
                    i(inv, 28, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 28, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 25, NETHER_STAR, "&2&lPlayer Visibility", 1, "&7Disable your ability to see;&7other players.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".visibility")) {
                    i(inv, 34, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 34, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 21, FEATHER, "&2&lSpeed", 1, "&7Speed around the lobby!");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".speed")) {
                    i(inv, 30, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 30, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 23, FEATHER, "&2&lFlight", 1, "&7Fly around the lobby!");
                if (player.getAllowFlight()) {
                    i(inv, 32, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 32, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
            case "helper":
            case "moderator":
            case "builder":
            case "artist":
                i(inv, 19, BOOK_AND_QUILL, "&2&lPlayer Chat", 1, "&7Disable your ability to see;&7other player's chat.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".chat")) {
                    i(inv, 28, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 28, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 25, NETHER_STAR, "&2&lPlayer Visibility", 1, "&7Disable your ability to see;&7other players.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".visibility")) {
                    i(inv, 34, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 34, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 21, SUGAR, "&2&lSpeed", 1, "&7Speed around the lobby!");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".speed")) {
                    i(inv, 30, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 30, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 23, FEATHER, "&2&lFlight", 1, "&7Fly around the lobby!");
                if (player.getAllowFlight()) {
                    i(inv, 32, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 32, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                break;
            case "youyuber":
            case "admin":
            case "developer":
            case "manager":
            case "owner":
                i(inv, 10, BOOK_AND_QUILL, "&2&lPlayer Chat", 1, "&7Disable your ability to see;&7other player's chat.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".chat")) {
                    i(inv, 19, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 19, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 16, NETHER_STAR, "&2&lPlayer Visibility", 1, "&7Disable your ability to see;&7other players.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".visibility")) {
                    i(inv, 25, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 25, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 12, SUGAR, "&2&lSpeed", 1, "&7Speed around the lobby!");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".speed")) {
                    i(inv, 21, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 21, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 14, FEATHER, "&2&lFlight", 1, "&7Fly around the lobby!");
                if (player.getAllowFlight()) {
                    i(inv, 23, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 23, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 29, SLIME_BALL, "&2&lRadius", 1, "&7Fling players away!");
                if (CommandRadius.map.containsKey(player)) {
                    i(inv, 38, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 38, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 33, SKULL_ITEM, "&2&lInvisibility", 1, "&7Make yourself invisible!", (short)0, false, player.getName());
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".invisibility")) {
                    i(inv, 42, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 42, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                break;
            default:
                i(inv, 20, BOOK_AND_QUILL, "&2&lPlayer Chat", 1, "&7Disable your ability to see;&7other player's chat.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".chat")) {
                    i(inv, 29, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 29, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                i(inv, 24, NETHER_STAR, "&2&lPlayer Visibility", 1, "&7Disable your ability to see;&7other players.");
                if (Main.getMainConfig().getBoolean("prefs." + player.getUniqueId() + ".visibility")) {
                    i(inv, 33, EMERALD_BLOCK, "&aEnabled", 1, ";&cClick to disable.");
                } else {
                    i(inv, 33, REDSTONE_BLOCK, "&cDisabled", 1, ";&aClick to enable.");
                }
                break;
        }
        player.openInventory(inv);
    }

    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing, String skullName) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Main.c(null, name));
        if (lore != null) {
            im.setLore(Arrays.asList(Main.c(null, lore).split(";")));
        }
        if (glowing) {
            im.addEnchant(Enchantment.DURABILITY, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        is.setItemMeta(im);
        inv.setItem(slot, is);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing) {
        i(inv, slot, type, name, amount, lore, data, glowing, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data) {
        i(inv, slot, type, name, amount, lore, data, false);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount, String lore) {
        i(inv, slot, type, name, amount, lore, (short)0);
    }
    private static void i(Inventory inv, int slot, Material type, String name, int amount) {
        i(inv, slot, type, name, amount, null);
    }
    private static void i(Inventory inv, int slot, Material type, String name) {
        i(inv, slot, type, name, 1);
    }
    private static void i(Inventory inv, int slot, Material type) {
        i(inv, slot, type, "");
    }

}
