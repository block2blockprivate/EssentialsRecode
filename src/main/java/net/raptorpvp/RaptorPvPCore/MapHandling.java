package net.raptorpvp.RaptorPvPCore;

import org.bukkit.entity.Player;

public class MapHandling {

    private String active;
    private Player playername;

    public void Message(Player player, String active) {
        this.playername = player;
        this.active = active;
    }

    public Player getPlayer() {
        return this.playername;
    }

    public String getActive() {
        return this.active;
    }
}
