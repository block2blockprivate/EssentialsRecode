package net.raptorpvp.RaptorPvPCore;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.line.TextLine;
import net.raptorpvp.RaptorPvPCore.listeners.TimerEvent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.*;

public class Parkour implements Listener,CommandExecutor {

    private static List<Location> checkLocations = new ArrayList<>();

    private Main m = Main.get();

    private static List<Player> parkourPlayers = new ArrayList<>();
    private static Map<Player, Double> parkourTimes = new HashMap<>();
    private static Map<Player, Integer> parkourChecks = new HashMap<>();

    private Location reset;
    private List<Location> checkpoints = new ArrayList<>();
    private static Location start;
    private static Location end;

    /*
    Gold = Checkpoint, Iron = Finish, Wooden = Start
     */

    public static void removeParkourPlayers(Player player) {
        if (parkourPlayers.contains(player)) {
            parkourPlayers.remove(player);
        }
    }

    public static boolean removeParkourPlayersBoo(Player player) {
        if (parkourPlayers.contains(player)) {
            parkourPlayers.remove(player);
            return true;
        }
        return false;
    }

    public static void removeParkourPTimes(Player player) {
        if (parkourTimes.containsKey(player)) {
            parkourTimes.remove(player);
        }
    }
    public static void setStart(Location l) {
        start = l;
    }
    public static void setEnd(Location l) {
        end = l;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!m.getConfig().getBoolean("settings.hubmode")) {
            sender.sendMessage(Main.c("Command Manager","&cThat command is unknown!"));
            return true;
        }
        if (sender instanceof Player) {
            if (args.length == 0) {
                sender.sendMessage(Main.c("Parkour", "Parkour Help:\n&a/parkour checkpoint &7- Teleports you to the last checkpoint you reached\n&a/parkour reset &7- Teleports you to the beginning of the parkour"));
                return false;
            }
            switch (args[0].toLowerCase()) {
                case "reset":
                    if (parkourPlayers.contains((Player) sender)) {
                        Location l = (Location) Main.getParkourFile().get("spawn.location");
                        ((Player) sender).teleport(l);
                    } else {
                        sender.sendMessage(Main.c("Parkour", "You have started the parkour in order to reset!"));
                    }
                    break;
                case "checkpoint":
                    if (parkourPlayers.contains((Player) sender)) {
                        if (parkourChecks.get((Player) sender).equals(0)) {
                            Location l = (Location) Main.getParkourFile().get("spawn.location");
                            ((Player) sender).teleport(l);
                        } else {
                            Location l = (Location) Main.getParkourFile().get(parkourChecks.get((Player) sender) + ".location");
                            ((Player) sender).teleport(l);
                        }
                        sender.sendMessage(Main.c("Parkour", "You have been teleported to your last checkpoint!"));
                    } else {
                        sender.sendMessage(Main.c("Parkour", "You have started the parkour in order to teleport to a checkpoint!"));
                    }
                    break;
                case "top3":
                case "leaderboard":
                    sender.sendMessage(Main.c("Parkour","The top 3 times are:\n&a#1 &7- &a" + m.getConfig().get("leaderboard.1.PlayerName") + " &7- &a" + m.getConfig().get("leaderboard.1.Time") + " &7seconds.\n&a#2 &7- &a" + m.getConfig().get("leaderboard.2.PlayerName") + " &7- &a" + m.getConfig().get("leaderboard.2.Time") + " &7seconds.\n&a#3 &7- &a" + m.getConfig().get("leaderboard.3.PlayerName") + " &7- &a" + m.getConfig().get("leaderboard.3.Time") + " &7seconds."));
                    break;
                case "setspawn":
                    if (((Player) sender).hasPermission("rank.admin")) {
                        if (((Player) sender).getLocation().getBlock().getType().equals(Material.WOOD_PLATE)) {
                            m.addParkourConfig("spawn.location", ((Player) sender).getLocation().getBlock().getLocation());
                            Location l = ((Player) sender).getLocation().getBlock().getLocation();
                            l.setY(l.getY() + 2);
                            l.setX(l.getX() + 0.5);
                            l.setZ(l.getZ() + 0.5);
                            Hologram hologram = HologramsAPI.createHologram(Main.get(), l);
                            TextLine textLine = hologram.appendTextLine(ChatColor.BLUE + "" + ChatColor.BOLD + "Parkour Start");
                            Main.getHolograms().add(hologram);
                        } else {
                            sender.sendMessage(Main.c("Parkour", "That location is not valid! Please stand over a wooden pressure plate to set the spawn."));
                        }
                    } else {
                        sender.sendMessage(Main.c("Parkour", "That subcommand is unknown!"));
                    }
                    break;
                case "setend":
                    if (((Player) sender).hasPermission("rank.admin")) {
                        if (((Player) sender).getLocation().getBlock().getType().equals(Material.IRON_PLATE)) {
                            m.addParkourConfig("end.location", ((Player) sender).getLocation().getBlock().getLocation());
                            Location l = ((Player) sender).getLocation().getBlock().getLocation();
                            l.setY(l.getY() + 2);
                            l.setX(l.getX() + 0.5);
                            l.setZ(l.getZ() + 0.5);
                            Hologram hologram = HologramsAPI.createHologram(Main.get(), l);
                            TextLine textLine = hologram.appendTextLine(ChatColor.BLUE + "" + ChatColor.BOLD + "Parkour End");
                            Main.getHolograms().add(hologram);
                        } else {
                            sender.sendMessage(Main.c("Parkour", "That location is not valid! Please stand over a iron pressure plate to set the end."));
                        }
                    } else {
                        sender.sendMessage(Main.c("Parkour", "That subcommand is unknown!"));
                    }
                    break;
                case "setrestart":
                    if (((Player) sender).hasPermission("rank.admin")) {
                        m.addParkourConfig("reset.location", ((Player) sender).getLocation());
                    } else {
                        sender.sendMessage(Main.c("Parkour", "That subcommand is unknown!"));
                    }
                    break;
                case "setcheck":
                    if (((Player) sender).hasPermission("rank.admin")) {
                        if (((Player) sender).getLocation().getBlock().getType().equals(Material.GOLD_PLATE)) {
                            m.addParkourConfig(args[1] + ".location", ((Player) sender).getLocation().getBlock().getLocation());
                            Location l = ((Player) sender).getLocation().getBlock().getLocation();
                            l.setY(l.getY() + 2);
                            l.setX(l.getX() + 0.5);
                            l.setZ(l.getZ() + 0.5);
                            Hologram hologram = HologramsAPI.createHologram(Main.get(), l);
                            TextLine textLine = hologram.appendTextLine(ChatColor.BLUE + "" + ChatColor.BOLD + "Checkpoint #" + args[1]);
                            Main.getHolograms().add(hologram);
                        } else {
                            sender.sendMessage(Main.c("Parkour", "That location is not valid! Please stand over a golden pressure plate to set a checkpoint."));
                        }
                    } else {
                        sender.sendMessage(Main.c("Parkour", "That subcommand is unknown!"));
                    }
                    break;
                default:
                    sender.sendMessage(Main.c("Parkour", "That subcommand is unknown!"));
                    break;
            }
        }
        return true;
    }


    public static List<Location> getCheckLocations() {
        return checkLocations;
    }


    @EventHandler
    public void timerEvent(TimerEvent e) {
        for (Player p : parkourPlayers) {
            double d = parkourTimes.get(p);
            parkourTimes.remove(p);
            parkourTimes.put(p, d + 1);
        }
    }



    @EventHandler
    public void onPressurePlate(PlayerMoveEvent e) {
        if (e.getFrom().getBlock().getType().equals(e.getTo().getBlock().getType())) {
            return;
        }
        if (!m.getConfig().getBoolean("settings.hubmode")) {
            return;
        }
        if (e.getPlayer().getLocation().getBlock().getType().equals(Material.WOOD_PLATE)) {
            if (e.getPlayer().getLocation().getBlock().getLocation().equals(start)) {
                if (parkourPlayers.contains(e.getPlayer())) {
                    e.getPlayer().sendMessage(Main.c("Parkour", "You have restarted the parkour! Your time has been reset to 0 seconds!"));
                    parkourTimes.remove(e.getPlayer());
                    parkourTimes.put(e.getPlayer(), 0.00);
                    parkourChecks.remove(e.getPlayer());
                    parkourChecks.put(e.getPlayer(), 0);
                } else {
                    e.getPlayer().sendMessage(Main.c("Parkour", "You have started the parkour!"));
                    e.getPlayer().setFlying(false);
                    parkourPlayers.add(e.getPlayer());
                    parkourTimes.put(e.getPlayer(), 0.00);
                    parkourChecks.put(e.getPlayer(), 0);
                }
            }
        } else if (e.getPlayer().getLocation().getBlock().getType().equals(Material.GOLD_PLATE)) {
            if (parkourPlayers.contains(e.getPlayer())) {
                Location l = e.getPlayer().getLocation().getBlock().getLocation();
                if (checkLocations.contains(l)) {
                    int i = checkLocations.indexOf(l) + 1;
                    if (i > parkourChecks.get(e.getPlayer())) {
                        parkourChecks.remove(e.getPlayer());
                        parkourChecks.put(e.getPlayer(), i);
                        e.getPlayer().sendMessage(Main.c("Parkour", "You reached checkpoint &a#" + i + "&7!"));
                    }
                }
            } else {
                e.getPlayer().sendMessage(Main.c("Parkour", "You must start the parkour to reach checkpoints!"));
            }
        } else if (e.getPlayer().getLocation().getBlock().getType().equals(Material.IRON_PLATE)) {
            if (e.getPlayer().getLocation().getBlock().getLocation().equals(end)) {
                if (parkourPlayers.contains(e.getPlayer())) {
                    if (m.getConfig().isSet("times." + e.getPlayer().getUniqueId() + ".time")) {
                        if (m.getConfig().getDouble("times." + e.getPlayer().getUniqueId() + ".time") > parkourTimes.get(e.getPlayer())/20) {
                            e.getPlayer().sendMessage(Main.c("Parkour", "You beat your previous record and completed the parkour in &a" + parkourTimes.get(e.getPlayer())/20 + "&7 seconds!"));
                            m.getConfig().set("times." + e.getPlayer().getUniqueId() + ".time", parkourTimes.get(e.getPlayer())/20);
                            m.saveYamls();
                        } else {
                            e.getPlayer().sendMessage(Main.c("Parkour", "You didn't beat your previous record! However, you completed the parkour in &a" + parkourTimes.get(e.getPlayer())/20 + "&7 seconds!"));
                        }
                    } else {
                        e.getPlayer().sendMessage(Main.c("Parkour", "Well done! You completed the parkour in &a" + parkourTimes.get(e.getPlayer())/20 + "&7 seconds!"));
                        m.getConfig().set("times." + e.getPlayer().getUniqueId() + ".time", parkourTimes.get(e.getPlayer())/20);
                        m.saveYamls();
                    }

                    if (m.getConfig().getDouble("leaderboard.1.Time") > parkourTimes.get(e.getPlayer())/20) {
                        if (e.getPlayer().getUniqueId().toString().equals(m.getConfig().get("leaderboard.1.UUID").toString())) {
                            m.getConfig().set("leaderboard.1.Time", parkourTimes.get(e.getPlayer())/20);
                            e.getPlayer().sendMessage(Main.c("Parkour","You are still 1st place on the leaderboard!"));
                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        } else {
                            if (!m.getConfig().get("leaderboard.2.UUID").toString().equals(e.getPlayer().getUniqueId().toString())) {
                                m.getConfig().set("leaderboard.3.Time", m.getConfig().getDouble("leaderboard.2.Time"));
                                m.getConfig().set("leaderboard.3.UUID", m.getConfig().get("leaderboard.2.UUID"));
                                m.getConfig().set("leaderboard.3.PlayerName", m.getConfig().get("leaderboard.2.PlayerName"));
                            }
                            m.getConfig().set("leaderboard.2.Time", m.getConfig().getDouble("leaderboard.1.Time"));
                            m.getConfig().set("leaderboard.2.UUID", m.getConfig().get("leaderboard.1.UUID"));
                            m.getConfig().set("leaderboard.2.PlayerName", m.getConfig().get("leaderboard.1.PlayerName"));

                            m.getConfig().set("leaderboard.1.Time", parkourTimes.get(e.getPlayer())/20);
                            m.getConfig().set("leaderboard.1.UUID", e.getPlayer().getUniqueId().toString());
                            m.getConfig().set("leaderboard.1.PlayerName", e.getPlayer().getName());
                            e.getPlayer().sendMessage(Main.c("Parkour","You are now 1st place on the leaderboard!"));
                            m.saveYamls();

                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        }
                    } else if (m.getConfig().getDouble("leaderboard.2.Time") > parkourTimes.get(e.getPlayer())/20) {
                        if (e.getPlayer().getUniqueId().toString().equals(m.getConfig().get("leaderboard.2.UUID").toString())) {
                            m.getConfig().set("leaderboard.2.Time", parkourTimes.get(e.getPlayer())/20);
                            e.getPlayer().sendMessage(Main.c("Parkour","You are still 2nd place on the leaderboard!"));

                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        } else {
                            m.getConfig().set("leaderboard.3.Time", m.getConfig().getDouble("leaderboard.2.Time"));
                            m.getConfig().set("leaderboard.3.UUID", m.getConfig().get("leaderboard.2.UUID"));
                            m.getConfig().set("leaderboard.3.PlayerName", m.getConfig().get("leaderboard.2.PlayerName"));

                            m.getConfig().set("leaderboard.2.Time", parkourTimes.get(e.getPlayer())/20);
                            m.getConfig().set("leaderboard.2.UUID", e.getPlayer().getUniqueId().toString());
                            m.getConfig().set("leaderboard.2.PlayerName", e.getPlayer().getName());
                            e.getPlayer().sendMessage(Main.c("Parkour","You are now 2nd place on the leaderboard!"));
                            m.saveYamls();

                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        }
                    }  else if (m.getConfig().getDouble("leaderboard.3.Time") > parkourTimes.get(e.getPlayer())/20) {
                        if (e.getPlayer().getUniqueId().toString().equals(m.getConfig().get("leaderboard.3.UUID").toString())) {
                            m.getConfig().set("leaderboard.2.Time", parkourTimes.get(e.getPlayer())/20);
                            e.getPlayer().sendMessage(Main.c("Parkour","You are still 3rd place on the leaderboard!"));

                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        } else {
                            m.getConfig().set("leaderboard.3.Time", parkourTimes.get(e.getPlayer())/20);
                            m.getConfig().set("leaderboard.3.UUID", e.getPlayer().getUniqueId().toString());
                            m.getConfig().set("leaderboard.3.PlayerName", e.getPlayer().getName());
                            e.getPlayer().sendMessage(Main.c("Parkour","You are now 3rd place on the leaderboard!"));
                            m.saveYamls();

                            parkourChecks.remove(e.getPlayer());
                            parkourTimes.remove(e.getPlayer());
                            parkourPlayers.remove(e.getPlayer());
                            return;
                        }
                    }
                    parkourChecks.remove(e.getPlayer());
                    parkourTimes.remove(e.getPlayer());
                    parkourPlayers.remove(e.getPlayer());
                    return;
                } else {
                    e.getPlayer().sendMessage(Main.c("Parkour", "You must start the parkour in order to finish it!"));
                }
            }
        }
    }

}
